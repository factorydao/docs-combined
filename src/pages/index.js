import React from "react";
import clsx from "clsx";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import HomepageFeatures from "@site/src/components/HomepageFeatures";
import {
  ArrowUpRight as LinkIcon,
  BookOpen,
  HelpCircle,
  Info,
  Package,
  Users,
  MessageCircle,
} from "react-feather";
import styled from "@emotion/styled";

import styles from "./index.module.css";

const Row = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 16px;
  justify-content: center;
  margin: 0 auto;
  padding: 1rem 0;
  max-width: 960px;

  @media (max-width: 960px) {
    grid-template-columns: 1fr;
    padding: 1rem;
    max-width: 100%;
    margin: 0 1rem;
  }
  @media (max-width: 640px) {
    grid-template-columns: 1fr;
  }
`;
const Card = styled.div`
  display: flex;
  height: 250px;
  max-height: 250px;
  padding: 1rem;
  flex-direction: column;
  justify-content: center;
  cursor: pointer;
  border: 1px solid transparent;
  border-radius: 20px;
  border: 1px solid var(--ifm-color-emphasis-200);
  /* flex: 1 1 0px; */

  &:hover {
    border: 1px solid var(--ifm-color-emphasis-400);
    box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.05);
  }

  @media (max-width: 960px) {
    width: 100%;
  }
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
`;
const ShadowCard = styled(Card)`
  ${'' /* box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.05); */}
  box-shadow: 2px 3px 10px rgb(155 155 155 / 50%);
  background-color: #ffffff1c;
  backdrop-filter: blur(10px);
  min-height: 200px;
  margin: 8rem 1rem 8rem;
  /* background-color: var(--ifm-color-emphasis-0); */
`;
const IconWrapper = styled.div`
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  margin-right: 0.5rem;
`;

const LinkIconWrapper = styled.div`
  opacity: 0.25;
`;
const DocsHeader = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  width: 100%;
  position: relative;
`;
const TopSection = styled.div`
  width: 100%;
  align-items: center;
  justify-content: space-between;
  display: flex;
  flex-direction: row;
  margin-bottom: 1rem;
`;

const LinkRow = styled.div`
  width: 100%;
  align-items: center;
  justify-content: space-between;
  display: flex;
  flex-direction: row;
  a h3 {
    color: black !important;
  }
`;

const SuiteRow = styled(Row)`
  margin-bottom: 5rem;
`;

export const suiteDapp = [
  {
    title: "Influence",
    text: "A system for decentralised decision making",
    to: "https://influence.factorydao.org/",
  },
  {
    title: "Mint",
    text: "An NFT minting suite",
    to: "https://mint.factorydao.org/",
  },
  {
    title: "Markets",
    text: "A prediction market and collective intelligence tool",
    to: "https://finance.vote/",
  },
  {
    title: "Launch",
    text: "A non-custodial crypto auction house",
    to: "https://launch.factorydao.org/",
  },
  {
    title: "Bank",
    text: "Infrastructure for token vesting",
    to: "https://bank.factorydao.org/#/",
  },
  {
    title: "Yield",
    text: "A liquidity mining system",
    to: "https://yield.factorydao.org/#/",
  },
];

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className={clsx("container", styles.heroBannerContainer)}>
        <h1 className="hero__title">{siteConfig.title}</h1>
      </div>
    </header>
  );
}

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <Container>
        <DocsHeader>

          <Row>
            {actions.map((action) => (
              <Link style={{ textDecoration: "none" }} to={action.to}>
                <ShadowCard key={action.title}>
                  <TopSection>
                    <IconWrapper>
                      <action.icon style={{ width: "24px" }} />
                    </IconWrapper>
                    <LinkIconWrapper>
                      <LinkIcon />
                    </LinkIconWrapper>
                  </TopSection>
                  <h3 style={{ marginBottom: ".75rem", fontWeight: 500 }}>
                    {action.title}
                  </h3>
                  <p style={{ marginBottom: "0.5rem", fontWeight: 300 }}>
                    {action.text}
                  </p>
                </ShadowCard>
              </Link>
            ))}
          </Row>
        </DocsHeader>


        <main>
          <SuiteRow>
            
              {suiteDapp.map((action) => (
                <Link key={action.to} element={action.to}>
                  <Link
                    style={{ textDecoration: "none" }}
                    key={action.title}
                    to={action.to}
                  >
                    <Card
                      key={action.title}
                      style={{ marginBottom: "1rem", height: "auto" }}
                    >
                      <LinkRow>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <h3 style={{ marginBottom: "0rem" }}>
                            {action.title}
                          </h3>
                        </div>
                        <LinkIconWrapper>
                          <LinkIcon />
                        </LinkIconWrapper>
                      </LinkRow>
                      <p style={{ marginBottom: "0rem", fontWeight: 300 }}>
                        {action.text}
                      </p>
                    </Card>
                  </Link>
                </Link>
              ))}
          </SuiteRow>
        </main>
      </Container>
    </Layout>
  );
}

export const actions = [
  {
    title: "Meet FactoryDAO",
    icon: Users,
    to: "/docs/Welcome/intro",
    text: `Learn about the core concepts of the Uniswap Protocol, Swaps, Pools, Concentrated Liquidity and more.`,
  },
  {
    title: "Read the Docs",
    icon: BookOpen,
    to: "/all-docs",
    text: `Learn how to integrate with Uniswap by building a dApp through guided examples.`,
  },
  {
    title: "Read FactoryDAO smart contracts",
    icon: Package,
    to: "https://gitlab.com/factorydao/contracts",
    text: `Learn about the architecture of the Uniswap Protocol smart contracts through guided examples.`,
  },
];
