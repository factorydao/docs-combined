import React from "react";
import Layout from "@theme/Layout";
import styles from "./all-docs.module.css";
import PageContainer from '@site/src/components/PageContainer';


  export default function Home() {
    return (
      <Layout>
        <main>
          <PageContainer />
        </main>
      </Layout> 
    );
  }