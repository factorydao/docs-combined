import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

export const suiteDappDocs = [
  {
    title: "Influence",
    text: "A system for decentralised decision making",
    to: "suite-docs/influence",
  },
  {
    title: "Mint",
    text: "An NFT minting suite",
    to: "https://mint.factorydao.org/",
  },
  {
    title: "Markets",
    text: "A prediction market and collective intelligence tool",
    to: "https://finance.vote/",
  },
  {
    title: "Launch",
    text: "A non-custodial crypto auction house",
    to: "https://launch.factorydao.org/",
  },
  {
    title: "Bank",
    text: "Infrastructure for token vesting",
    to: "https://bank.factorydao.org/#/",
  },
  {
    title: "Yield",
    text: "A liquidity mining system",
    to: "https://yield.factorydao.org/#/",
  },
];

const FeatureList = [
  {
    title: "Influence",
    logo: require("../../../static/images/logos/influence_icon.png").default,
    to: "../docs/suite-docs/mint/welcome",
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
  {
    title: "Influence2",
    logo: require("../../../static/images/logos/influence_icon.png").default,
    to: "https://mint.factorydao.org/",
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
  {
    title: "Influence2",
    logo: require("../../../static/images/logos/influence_icon.png").default,
    to: "https://mint.factorydao.org/",
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
  {
    title: "Influence",
    logo: require("../../../static/images/logos/influence_icon.png").default,
    to: "../docs/suite-docs/mint/welcome",
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
  {
    title: "Influence2",
    logo: require("../../../static/images/logos/influence_icon.png").default,
    to: "https://mint.factorydao.org/",
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
  {
    title: "Influence2",
    logo: require("../../../static/images/logos/influence_icon.png").default,
    to: "https://mint.factorydao.org/",
    description: (
      <>
        Docusaurus was designed from the ground up to be easily installed and
        used to get your website up and running quickly.
      </>
    ),
  },
];

function DocumentationLink({ logo, title, description, to }) {
  return (
      <div className={styles.mainContainer}>
          <img className={styles.featureSvg} src={logo} />
        <div className={styles.textCenter}>
          <h3>
            <a href={to}>{title}</a>
          </h3>
          <p>{description}</p>
        </div>
      </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
        <div className={styles.cardDocs}>
          {FeatureList.map((props, idx) => (
            <DocumentationLink key={idx} {...props} />
          ))}
        </div>
    </section>
  );
}
