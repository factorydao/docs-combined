import React, { useEffect, useState } from "react";
import styles from "./styles.module.css";

const FeatureList = [
  {
    title: "Influence",
    id: 'influencetile',
    logo: require("../../../static/images/logos/influence_icon.png").default,
    gradientColors:
      "rgb(32, 242, 236, 0.4), rgb(69, 184, 255, 0.4), rgb(69, 184, 255, 0.4)",
    to: "../docs/suite-docs/influence/welcome",
    description: (
      <>
        A robust governance system for decentralised decision making optimised
        for NFT governance in MetaDAO / SubDAO structures and social
        coordination. With Metaverse identity gating, quadratic voting and
        integrated proposal building.
      </>
    ),
  },
  {
    title: "Markets",
    id: 'marketstile',
    logo: require("../../../static/images/logos/markets_icon.png").default,
    gradientColors:
      "rgb(0, 220, 200, 0.4), rgb(0, 177, 255, 0.4), rgb(31, 111, 229, 0.4)",
    to: "../docs/suite-docs/markets/welcome",
    description: (
      <>
        The cryptoeconomic game that marries governance with the markets.
        Make market predictions by voting on assets from across the cryptospace and beyond. 
      </>
    ),
  },
  {
    title: "Mint",
    id: 'minttile',
    logo: require("../../../static/images/logos/mint_icon.png").default,
    gradientColors:
      "rgb(28, 201, 255, 0.4), rgb(133, 54, 255, 0.4), rgb(77, 18, 255, 0.4)",
    to: "../docs/suite-docs/mint/welcome",
    description: (
      <>
        An NFT minting suite purpose built for DAOs with dynamic whitelisting
        and pricing using merkle tree cryptography. The NFT price discovery
        system for effective NFT and DAO launches.
      </>
    ),
  },
  {
    title: "Launch",
    id: 'launchtile',
    logo: require("../../../static/images/logos/launch_icon.png").default,
    gradientColors:
      "rgb(253, 244, 70, 0.4), rgb(255, 88, 36, 0.4), rgb(218, 0, 96, 0.4)",
    to: "../docs/suite-docs/launch/welcome",
    description: (
      <>
        A non-custodial price discovery auction system that allows DAOs to issue
        tokens from their treasury to bootstrap liquidity or capitalise with
        non-network assets.
      </>
    ),
  },
  {
    title: "Yield",
    id: 'yieldtile',
    logo: require("../../../static/images/logos/yield_icon.png").default,
    gradientColors:
      "rgb(184, 226, 136, 0.4), rgb(125, 226, 19, 0.4), rgb(0, 146, 69, 0.4)",
    to: "../docs/suite-docs/yield/welcome",
    description: (
      <>
        A liquidity mining system that allows DAOs to control the emission of
        their tokens over time. The P2E yield farming experience allowing pro
        players to win against the whales.
      </>
    ),
  },
  {
    title: "Bank",
    id: 'banktile',
    logo: require("../../../static/images/logos/bank_icon_dark.png").default,
    logoDark: require("../../../static/images/logos/bank_icon_light.png").default,
    gradientColors: "rgb(255, 255, 255, 0.4), rgb(0, 0, 0, 0.4)",
    to: "../docs/suite-docs/bank/welcome",
    description: (
      <>
        A trustless token vesting and DAO payroll system with airdrop, linear
        vesting and variable vesting solutions for up to millions of
        participants.
      </>
    ),
  },
];

function DocumentationLink({ logo, title, description, to, gradientColors, id }) {
  return (
    <div class={styles.columnA}>
      <a href={to}>
          <div
            className={styles.singleCard}
            style={{
              backgroundImage: `linear-gradient( ${gradientColors} ), linear-gradient(90deg, rgba(120, 120, 120,0.5) 70%, rgba(120, 120, 120,0.5) 70%), url(${logo})`,
            }} id={id}
          >
            <div>
              <h3>
                <a className={styles.titleLink}>{title}</a>
              </h3>
              <p>
                {description}
              </p>
            </div>
          </div>
      </a>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <div className={styles.features}>
      <div className={styles.rowA}>
        {FeatureList.map((props, idx) => (
          <DocumentationLink key={idx} {...props} />
        ))}
      </div>
    </div>
  );
}
