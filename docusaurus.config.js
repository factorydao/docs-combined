// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'FactoryDAO',
  tagline: 'Build a DAO now',
  favicon: 'img/fdaoLogo.ico',

  // Set the production url of your site here
  url: 'https://docs.factorydao.vercel.app/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true, 
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'Home',
        logo: {
          alt: 'FactoryDAO Logo',
          src: 'img/FDAO_b_im.png',
          srcDark: 'img/fdaoLogo.png',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'start',
            position: 'left',
            label: 'Start',
          },
          {to: '/blog', label: 'Blog', position: 'right'},
          {
            type: 'docSidebar',
            sidebarId: 'influence',
            position: 'left',
            label: 'Influence',
          },
          {
            type: 'docSidebar',
            sidebarId: 'bank',
            position: 'left',
            label: 'Bank',
          },
          {
            type: 'docSidebar',
            sidebarId: 'launch',
            position: 'left',
            label: 'Launch',
          },
          {
            type: 'docSidebar',
            sidebarId: 'markets',
            position: 'left',
            label: 'Markets',
          },
          {
            type: 'docSidebar',
            sidebarId: 'mint',
            position: 'left',
            label: 'Mint',
          },
          {
            type: 'docSidebar',
            sidebarId: 'yield',
            position: 'left',
            label: 'Yield',
          },
          {
            href: 'https://gitlab.com/factorydao',
            label: 'GitHub',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'All Docs',
                to: '/all-docs',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Stack Overflow',
                href: 'https://stackoverflow.com/questions/tagged/docusaurus',
              },
              {
                label: 'Telegram',
                href: 'https://t.me/factdao',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/FactDAO',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitHub',
                href: 'https://gitlab.com/factorydao',
              },
              {
                label: 'Page',
                href: 'https://www.factorydao.org/'
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} FactoryDAO, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
