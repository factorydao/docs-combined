---
sidebar_position: 1
id: introduction
title: Introduction
---

The cryptospace is a proving ground for genuinely new democratic models. With the emergence of the decentralised finance (DeFi) movement, new and radical ways of reaching consensus and coordinating around money are being created with rapid open innovation.

Although early DeFi applications have been active since 2017, it is the emergence of governance tokens that has caused explosive growth in the DeFi space. They are the mechanism by which true decentralisation can be achieved, pushing trust away from contract creators and onto token holders.

They are however, largely flawed in their implementation. Highly asymmetric token distributions, voter apathy [1] and huge gaps between the technical knowledge of core teams and token holders creates a context where genuine governance is surface level at best and thinly veiled centralisation at worst. [2]

As a plethora of new tokens enter the market, it becomes increasingly difficult to keep up with new technological developments, but also separate out quality projects from low quality clones, or outright scams. Market signalling is a primary economic cost in the cryptospace and finance.vote will allow users and entrepreneurs to identify the impact of their signalling activity as well as provide early access to market signals.

finance.vote is a decentralised application for reaching consensus across the cryptospace [3] as a whole. It provides a space for users to engage with market discovery on new and existing tokens and be incentivised to share their perception on future price action.

Finance.vote has three cryptoeconomic components:
* Prediction and Market Discovery.
* Second Layer Governance.
* Decentralised Social Trading.


[1] “Blockchain Voter Apathy. Governance is a key area of ....” 29 Mar. 2019, https://medium.com/wave-financial/blockchain-voter-apathy-69a1570e2af3. *Accessed 26 Aug. 2020.*<br/>
[2] “Deconstructing ‘Decentralization’: Exploring the Core Claim of ....” 13 Feb. 2019, https://www.ssrn.com/abstract=3326244. *Accessed 26 Aug. 2020.*<br/>
[3] “A signaling theory model of cryptocurrency issuance and value.” https://ethresear.ch/t/a-signaling-theory-model-of-cryptocurrency-issuance-and-value/1081. *Accessed 26 Aug. 2020.*<br/>