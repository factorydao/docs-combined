---
sidebar_position: 7
id: secondlayergovernance
title: Second Layer Governance
---
## Bringing Politics to DeFi
Problem
* Many established networks lack any formal governance mechanism to receive accurate signals from their token holders regarding the future direction of the network.
* Loose and chaotic social signalling systems, such as Telegram and Reddit are ineffective for reaching consensus, are prone to spam and are an inconvenience for teams.
* DeFi DAOs are often heavily gated and cornered by asymmetric token issuances, meaning that users have no meaningful influence on the network.
* Permissionless systems allow projects to be easily cloned and users can be duped into buying fake tokens or engaging with projects that are outright scams [10].


## Introduction
Governance is what allows you to get things done as a collective. It is the process of making decisions about decisions. It is the act of generating effective processes that allow people to organise at scale and move towards a productive outcome. It is the reason why we have the modern civilisation that we have today, allowing us to form coherent social self-organised structures that churn out innovation and productivity. And now we are doing it on the internet.

It can not be underestimated how important this transition is. Typically, the power to organise and work towards a productive outcome has taken place within the confines of institutions, organisations and firms, but now humans are coalescing in digital spaces and forming movements. They are not very productive yet and typically fall prey to the tyranny of structurelessness, but with blockchain based governance systems that could change.

The missing piece for decentralised global organisations is voting technology. Outside of the confines of institutional governance, there is no control of the narrative. No way to keep conversation on track towards a common goal. The establishment of norms within a group becomes tricky, as self-interested parties, consciously or otherwise, drag discourse towards their own goals. What they are missing is a means to reach consensus.

The finance.vote second layer governance system uses the construction of decentralised financial systems to experiment with ways to use voting technologies to experiment with social consensus formation.



## Hierarchical Governance
Governance at the protocol layer is a hotly contested narrative across the cryptospace [11]. Cryptonetworks require consistency, transparency and incredibly high levels of security and this does not lend itself to particularly agile governance structures. Some networks have attempted to innovate beyond this and in many cases it has led to corruption [12], collusion [13] and sometimes outright failure [14] of the tokens resulting in permanent loss of funds.

More recently, Decentralised Autonomous Organisations (DAOs) have become increasingly prominent in emerging crypto networks, especially in the DeFi space. They can allow substantial changes to take place, pivoting to new contracts and shifting monetary policy in ways that can have a large impact on token holders. They are risky and should be used sparingly.

We propose a multi-level hierarchical approach to governance, which separates high stakes reality altering decisions to a highly secure, scope limited “layer one DAO”. Thus, leaving the full gamut of decision making formation arising from dialogue and rough consensus formation to layer two miniDAOs.



## Lobbying and Social Consensus Formation
Up until now, DAOs have been poor places to engage in complex decision making. Typically, they are substantially gated, requiring sometimes exceptionally high token stakes to demonstrate any meaningful voice [15].

The finance.vote second layer governance system is designed to allow social signals to form, be amplified and captured in token specific miniDAOs. These systems will demonstrate the first use of the semantic ballot voting system outside of asset price discovery. Here, the system will be turned towards dialogue, content curation and decision making.

It is the intention of this system to provide a space that users can reach consensus on what it is that they desire their chosen networks to focus on and move towards. It will be a space to share research, build knowledge and reach shared meaning.
Our semantic ballot voting system will utilise quadratic voting to build decentralised curation processes. Decisions and proposals that receive sufficient consensus will be able to transition to the layer one DAO, the DMF for consideration.


## miniDAOs
The second layer governance system is built to convert rough consensus and dialogue into actionable decisions in both the layer one DAOs of the finance.vote.

With the integration of semantic ballot voting, miniDAOs are dialogic spaces driven by cryptoeconomics. Users will be able to create their own votes, vote on discussion topics and curate ideas and sources.

The key to these spaces is content sorting. Users will utilise voting tools, that are live and battle tested in the adversarial environment of our vote markets. Finance.vote therefore sits in the governance space in between the extremes of high-stakes, on-chain governance and the loose social consensus formation in channels such as Reddit and Telegram.

## Blockchain Agnostic
As a governance system in the emerging DeFi space, blockchain interoperability is paramount.

Finance.vote will begin life on the Ethereum platform and will always have Ethereum based components, however, our second layer governance system will have several forked development trajectories across a range of blockchains; the miniDAOs is where these development trajectories are realised.

In order to bootstrap and showcase the second layer governance system, the finance.vote launch team commit to deploying the following miniDAOs:

* The $FVT miniDAO: Ethereum based.
* A $BTC miniDAO: Bitcoin based.
* One other; TBC

This launch set of miniDAOs will be deployed during the Pyramid phase of the network. After this point, new miniDAOs will be released in tranches to blockchains who wish to spin up a decision making structure using network infrastructure. These miniDAO slots will be auctioned as DITs via the auction.vote system and development for them will be funded by the DMF treasury.

## Scalable voting
The gas limitations in our vote markets are a fundamental component in their cryptoeconomics. These network costs imply a base economic value for the $FVT economy. If users are willing to pay these fees in order to obtain $FVT, then $FVT has some value delta above that base cost.

However, in our second layer governance system we want dialogue; curated on-chain chatter. This needs to be as cost effective as possible, but maintain the necessary degrees of sybil resistance to keep these space spam free and the voting systems effective.

Consequently, we hereby open a funded multi-year research project to develop scalable voting systems for deployment in our miniDAOs. This will include the formation of private voting solutions, whereby user identity can be selectively disclosed based on user choice.

The most high volume vote markets will be set up as “bake off” environments to test the latest on-chain scalability solutions, with the explicit focus of voting technology.

## Plutocracy
1 coin, 1 vote systems (1c1v), perhaps inevitably, trend towards extreme plutocracy.

Highly uneven distribution of tokens are a feature of all token economies up to this point and this creates a significant issue in governance systems, as wealth turns from merely economic power, to coordinative power.

A central tenet of the finance.vote network is to explore mechanisms that break this direct correlation between wealth and power.

This is done though the use of the identity linked vote power system, which in the vote markets is generated through meritocratic means. This alone, will generate a separation between token holdings and influence within the system and our main decision making architecture will include both a wealth based and meritocracy based dynamics.

This dynamic is extended and nuanced in the second layer governance system, which utilises the .vote consensus system and the introduction of tuneable token stake mechanics that are cross-chain and blockchain specific.

## The .vote consensus mechanism
The .vote consensus mechanism is the means through which token stake is used to weight voting power in each of the miniDAOs in the finance.vote ecosystem. It utilises a pyramidic stacking mechanism to normalise vote power across a voting population, ensuring that large token holders do not have an extremely out weighted voice in the system.

<p align="center">

<img height="300" src={require("../../../static/images/vote_consensus_mechanism.png").default}/>
</p>


Users are stacked in layers into staking slots according to the Fibonacci sequence.
This status dynamic ensures that token holders can utilise their wealth to increase their influence, but not so unduly that they dominate the system to the point of corruption. The interplay between users’ $V balance will ensure that vote power is optimised to avoid plutocracy.

A single user with the highest token stake will have the highest weighted voice in that miniDAO. Token balances thereafter determine which tier of the consensus mechanism that they sit within. Each tier has a number of staking slots, which are populated by users based on their staked token balances. Each tier has the same staking power, but is shared between a greater number of people. The tiers scale in slot size out to infinity.

For example, a user with 100,000 $FVT tokens staked on the $FVT miniDAO has the highest stake on that node. The next nearest token balances are 95,000, 90,000 and 80,000. The 95,000 and the 90,000 stakers occupy the second staking tier and the 80,000 staker takes the third slot along with two others. The 100,000 staker has 1,000 $V and the 95,000 and 90,000 stakers have 500 $V, and next tier down have 333 $V and so on.

This mechanism incentivises users to purchase $FVT to stake in their chosen miniDAOs to raise their influence in the social consensus formation process, but mitigates plutocratic power formation.

## Summary
In summary, the finance.vote second layer governance system is an adaptive pseudo- hierarchical DAO architecture that uses voting technology to experiment with new forms of human coordination.


[10] “Fake Tokens Continue to Plague Uniswap - Cointelegraph.” 19 Aug. 2020, https://cointelegraph.com/news/fake-tokens-continue-to-plague-uniswap. *Accessed 26 Aug. 2020.*<br/>
[11] “Against on-chain governance. Refuting (and rebuking) Fred ....” 30 Nov. 2017, https://medium.com/@Vlad_Zamfir/against-on-chain-governance-a4ceacd040ca. *Accessed 26 Aug. 2020.*<br/>
[12] “Tron’s Takeover of Steemit Is Internet History Repeating Itself ....” 5 Mar. 2020, https://www.coin-desk.com/trons-takeover-of-steemit-is-internet-history-repeating-itself. *Accessed 26 Aug. 2020.*<br/>
[13] “Leak reveals collusion on EOS blockchain - The Block.” 1 Oct. 2018, https://www.theblockcrypto.com/linked/1015/leak-reveals-collusion-on-eos-blockchain. *Accessed 26 Aug. 2020.*<br/>
[14] “YAM Finance Crashes Over 90%, Founder Admits His Failure.” 13 Aug. 2020, https://cryptopotato.com/yam-finance-crashes-over-90-founder-admits-his-failure/. *Accessed 26 Aug. 2020.*<br/>
[15] The Compound governance token ($COMP) requires 1% of the total token supply delegation to sub- mit a proposal, currently valued at $16.7m. https://compound.finance/docs/governance#introduction<br/>