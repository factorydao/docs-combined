---
sidebar_position: 4
id: contractaddresses
title: Contract Addresses
---

- **uniswap** = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
- **safeAddress** = '0xa874Fa6ccDcCB57d9397247e088575C4EF34EC66'

## Testnets
#### Rinkeby

- **rinkebySafeMathLib** = '0x0cd13D6731F9b3f02DC31250b537cC2284800402'
- **rinkebyToken** = '0x1999b48df6Da3fa7810b3cE3fCE3b1da4E819888'
- **rinkebyUniswapLP** = '0x0681e248a813ee6144c21fddc05924b2baaf942c'


## Mainnet
#### Libraries

- **mainnetSafeMath** = '0x82d7630c5EB722557De6D76575C9a7b8DE718500'
- **mainnetMerkleLib** = '0x8196D6264BB667908dC106D855Bf53E03816e725'

#### mint
- <span class="deprecatedText">mainnetIncinerator = '0x8C3D1656F22Ce2217C6A87200fa29f01CC3CA5A2' --- deprecated</span>
- <span class="deprecatedText">mainnetIncinerator = '0x83aDE5216489E4768B80227C4608C5B12179dE4d' -- deprecated</span>
- <span class="deprecatedText">mainnetFixedPriceGate = '0xff880DDa00485E12F5733E46862fD7b391eB813F' --- deprecated</span>
- <span class="deprecatedText">mainnetFixedPriceGate = '0xc94CD0479d5e57BaE2Bed04Bc816BFFb06e626E8' -- deprecated</span>
- <span class="deprecatedText">mainnetGatedMerkleIdentity = '0xfb0E8ff1deB51a76E4a623A4f839Aa3a5Ce786Be' --- deprecated</span>
- <span class="deprecatedText">mainnetGatedMerkleIdentity = '0xb83B063838Cdb56F25D778D771390cdFd307938D' --- deprecated</span>
- <span class="deprecatedText">mainnetGatedMerkleIdentity = '0x1f531048263d8E68c3bDE21a410bF8F0B65d414d' -- deprecated</span>
- <span class="deprecatedText">mainnetVotingIdentity2 = '0x33aBDF51C51173382818ba719Cb1886eC40e160c' --- deprecated</span>
- <span class="deprecatedText">mainnetVotingIdentity2 = '0xD5B81D548028710fE982E069A995e6C7500CD5E7' -- deprecated</span>
- <span class="deprecatedText">mainnetVotingIdentity2 = '0x2eB92337f831b3F0A00F66c98622bC713F71613d' -- deprecated</span>
 

#### bank
- <span class="primaryText">mainnetTeamVault = '0x3e9109e38eDfEdFdCd5350672C287E2C672C9E2D' -- primary</span>
- **mainnetInvestorVault** = '0xB29B62e1Bf13f2F41960fEd1853032849b449673'
- **mainnetAdvisorVault1** = '0x40dEF72cE904E90684f5E4677c354F1E676d3373'


#### influence
- <span class="primaryText">mainnetIdentity = '0xf779cae120093807985d5F2e7DBB21d69be6b963' -- primary</span>
- **mainnetLondon2021Identity** = '0x6b30DfAAc56De970d5040A8727A8d26F98020447'


#### launch
- <span class="primaryText">mainnetAuction = '0x7A4bb7c12354780822d4Ed23114274be4E4C8E83' -- primary</span>
- **mainnetFVT** = '0x45080a6531d671DDFf20DB42f93792a489685e32'
- <span class="emptyText">mainnetLPVault = '0xC7900783578E026645A6FCFDB7aa26adb63160E2' -- empty</span>
- **mainnetLPUniswap** = '0x75001b3ffe0f77864c7dc64c55e1e22b205e4a07'
- **mainnetLPSushiswap** = '0x96335e7cdbcd91fb33c26991b00cc13a87a811b9'


#### markets
- <span class="primaryText">mainnetTrollbox = '0xEa6556E350cD0C61452a26aB34E69EBf6f1808BA' -- primary</span>
- **mainnetTrollboxProxy** = '0xF9D234773ae2cE14277A9026fF6DA340669FdE4e'
- **mainnetReferralProgram** = '0x798A709E12CcA28bFa7Ff4a6dAa044b5e0B5FA00'
- **mainnetMKRAggregator** = '0x63A9ba6fdCcb08992c4886b8162468ABF2C240eF'
- **mainnetMKRAggregator2** = '0x0309B42c1DC9Ed9d95c06Aa32646Eea1Ca232d95'
- **mainnetUMAAggregator** = '0x4BC84c91f5Fe1052Da563bB327B639ebD4469d63'
- **mainnetUMAAggregator2** = '0x8e674727e7e53bdAbBC9C0Dc844a36a2163e6c6A'
- <span class="deprecatedText">mainnetChainlinkOracle = '0xc9EE4C2e16E9BaA0A10031E082EB3D7aFd94E75e' -- deprecated</span>
- **mainnetChainlinkOracle2** = '0xa58d366f2078900E76Bea7f3dBb64766BB9614f4'
- **mainnetKeeperRegistry** = '0x5C8b4D52683758CF855Fa2118Ef0104FdCD63698'
- **mainnetKeeperRegistry2** = '0x109A81F1E0A35D4c1D0cae8aCc6597cd54b47Bc6'


#### bridge
- <span class="primaryText">mainnetChainBridge = '0xe3Dd2b16D9Db5d59Ea17c8e8c0A3e11e6C97b248' -- primary</span>
- **mainnetBridgeERC20Handler** = '0x42876633c355C8043d0872BCD85c73325dE08C13'


#### yield
- **mainnetMostBasicYield** = '0x38A1f049b61024316969a1559F4F093391b1dEF6'
- <span class="deprecatedText">mainnetLiquidityMining = '0x3Ff8338CA0fEEB4c950D78f5A5C00bdF104078CE' -- deprecated</span>
- <span class="deprecatedText">mainnetLiquidityMining = '0x4F66f085727C3d8b72FFD2Efd0Bb93469648f945' -- deprecated</span>
- <span class="deprecatedText">mainnetLiquidityFactory = '0xF94E4ee9a0ACA2193c0E9d38F60202382eAfBF4F' -- deprecated</span>
- <span class="primaryText">mainnetLiquidityFactory = '0xfe64d9A8Fd6565b6842d6574871D929809424280' -- primary</span>
- **mainnetBasicPoolFactory** = '0x26D853B680e28d33e3Df80C566027452bB32e9B3'


#### partners
- **mainnetITrustMostBasicYield** = '0xe15a31e865B7C5769f69493AEE08aF2FF27CE39d'



## Binance Smart Chain

#### libraries
- **bscSafeMathLib** = '0x86b43bf8bA2B61eA3c3F5a3C4c07517077fA043D'

#### bridge
- **bscChainBridge** = '0xB8f5496Ca6A013622Ab909E7fE7d22b7Bc442213'
- **bscBridgeERC20Handler** = '0x785CFC6C2afcB058E8Dd6DDA49537C5a819D3625'
- **bscBridgeRelayer** = '0x2E70f28C39c8d1f1b737cD37e1EC76b72bD87cf6'
  
#### markets
- **bscIdentity** = '0x951AED5E3554332BC2624D988c9c70d002D3Dba0'
- **bscTrollbox** = '0xA6Bb733a61f2f4F09090781CCCD80929c9234b3f'
- **bscTrollboxProxy** = '0xaDd34aCdbFc4733e5a448010F5cc809FF99e17aD'
- **bscReferralProgram** = '0x82d7630c5EB722557De6D76575C9a7b8DE718500'
- **bscChainlinkOracle2** = '0xf779cae120093807985d5F2e7DBB21d69be6b963'

#### infrastructure
- <span class="deprecatedText">bscFVT = '0xc669E7fc84Fee4017782E67BcC45e5D7F65566BD' -- deprecated</span>

- <span class="deprecatedText">bscFVT2 = '0x447047D2AC13ADD7BbA443f177A576852dD08Eb8' -- deprecated</span>
- <span class="deprecatedText">bscFVT3 = '0x6E9630a7388E57D4457e5750388EB665a36B9cfA' -- deprecated</span>
- **bscFVT4** = '0x0A232cb2005Bda62D3DE7Ab5DEb3ffe4c456165a'
- <span class="deprecatedText">bscLP = '0xfe2193a3b487847e107426927a1f6fc32b38dbae' -- deprecated</span>
- <span class="deprecatedText">bscLP2 = '0x33a23558eb1ef3139dcf3a0484ad59c6e9755beb' -- deprecated</span>
- **bscLP3** = '0x668765Fd5Ce2F9f66a27BBBCa76c129aA8C45D90'

## Polygon
- **maticSafeMathLib** = '0x86b43bf8bA2B61eA3c3F5a3C4c07517077fA043D'
- **maticChainBridge** = '0xB8f5496Ca6A013622Ab909E7fE7d22b7Bc442213'
- **maticFVT** = '0x72a5a58f79FFc2102227B92fAeBA93B169a3A3F1'
- **maticBridgeERC20Handler** = '0x785CFC6C2afcB058E8Dd6DDA49537C5a819D3625'
- **maticBridgeRelayer** = '0x2E70f28C39c8d1f1b737cD37e1EC76b72bD87cf6'
- **maticLP** = '0xBdeD75E911418ae4ECB117724B4b88458e3De88b'


