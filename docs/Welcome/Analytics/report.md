---
sidebar_position: 3
id: votemarketsreport
title: A report on the Vote Markets 
---
September 2021

-----------------------------

**An analysis by @LIzzl Datadeo#8507**. For original analytics see [this pdf file](../../../static/images/analytics/FV_vote_markets_analysis.pdf).

Users have voted over 37 rounds to predict the market performance of Defi tokens using a new voting mechanism called Quadratic Voting.<br/>
This report analyses the information that was compiled from those votes. The main questions answered are:
- How did users interact with the platform?
- How did voters apply Quadratic Voting?
- What makes a user a successful predictor?
- What is the predictive power of markets.vote?

## What is Quadratic Voting?
That's a voting strategy that helps balance the voting power of big players and small ones. The idea standing behind QV mechanism is to make each additional vote cost a lot more than the previous one. <br/>
It's worth to remember that in QV participants vote for or against an issue, but also express how strongly they feel about it.<br/>
The table down below depicts how cost in tokens depends on the number of votes:<br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th>Number of votes</th>
        <th>Cost in Tokens</th>
    </tr>
    <tr>
        <td>1</td>
        <td>1</td>
    </tr>
    <tr>
        <td>2</td>
        <td>4</td>
    </tr>
    <tr>
        <td>3</td>
        <td>9</td>
    </tr>
    <tr>
        <td>4</td>
        <td>16</td>
    </tr>
    <tr>
        <td>5</td>
        <td>25</td>
    </tr>
    <tr>
        <td>6</td>
        <td>36</td>
    </tr>
    <tr>
        <td>7</td>
        <td>49</td>
    </tr>
    <tr>
        <td>8</td>
        <td>64</td>
    </tr>
    <tr>
        <td>9</td>
        <td>81</td>
    </tr>
    <tr>
        <td>10</td>
        <td>100</td>
    </tr>

</table>
</div>

More on quadratic voting: [click here](/docs/Welcome/Whitepaper/tokeneconomics#quadratic-voting) or [here](/docs/Welcome/Whitepaper/votemarket#quadratic-voting)



## How does the markets.vote work?
Users are incentivized to make market predictions in a series of tournaments focussed on a basket of crypto assets. <br/>
Quadratic voting is used to generate a consensus in a perceived future market order. Users get a default voting power of 100$V. They then spend a budget of voting power to create a new order, based on their perception of token quality and future potential market performance.

Users are rewarded with a proportional share of a network-generated reward pool depending on the proportionality of their correctness.<br/>
Users can amplify their voting power beyond the starting level by demonstrating a history of correct decision-making in the markets, or by purchasing more identities.

More on markets.vote in [the whitepaper](/docs/Welcome/Whitepaper/votemarket). Check out also [markets app](https://finance.vote/#/vote/how-it-works).




## Voter Activity

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="2">How did users interact with vote markets?</th>
    </tr>
    <tr>
        <td>Total voters</td>
        <td>298</td>
    </tr>
    <tr>
        <td>Total votes</td>
        <td>1379</td>
    </tr>
    <tr>
        <td>Total voterIDs</td>
        <td>597</td>
    </tr>
    <tr>
        <td>Average votes per round</td>
        <td>38</td>
    </tr>
    <tr>
        <td>Average user would mint</td>
        <td>2 voterIDs</td>
    </tr>
    <tr>
        <td colspan="2">No voterID has ever been transfered  </td>
    </tr>
</table>
</div>

<p align="center">

![FV8](../../../static/images/analytics/FV_share8.jpg)
</p>

<p align="center">

![FV9](../../../static/images/analytics/FV_round9.jpg)
</p>


## Voter turnout

<div class="sideTosideWrapperIMG500">
<div>
<img src={require("../../../static/images/analytics/FV_voterturnout10.jpg").default}/>
</div>
<div>
    Voter turnout with <strong>transaction cost</strong>*<br/>
    <br/>
    *average amount of ETH spent per transaction
</div>
</div>



## How did voter apply QV?

<div class="sideTosideWrapper">
<div>
<img class="" src={require("../../../static/images/analytics/FV_votingpower12.png#bitcoinImages20").default}/>
</div>
<div>
        On average, voters used <strong>72%</strong> of their voting power per vote. <br/>
        <strong>100, 125, 97</strong> were three most used amounts of voting power.<br/><br/>
        On average, voters made <strong>5</strong> choices per vote. <br/>
        With <strong>10, 4, 1</strong> being the most used number of choices.
</div>
</div>


<p align="center">

![FV_weightcombination14](../../../static/images/analytics/FV_weightcombination14.jpg)
</p>



## Coin clairvoyants

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 wallets with most Voting Power    </th>
    </tr>
    <tr>
        <td>Address</td>
        <td>0x34...39EA</td>
        <td>0x6e...dDec</td>
        <td>0x67...e640</td>
    </tr>
    <tr>
        <td>Voting Power</td>
        <td>5400</td>
        <td>4406</td>
        <td>3501</td>
    </tr>
    <tr>
        <td># of voterID</td>
        <td>50</td>
        <td>5</td>
        <td>15</td>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>3</td>
        <td>26</td>
        <td>16</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>95.9%</td>
        <td>45.9%</td>
        <td>61.3%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>4.1</td>
        <td>3.7</td>
        <td>9.9</td>
    </tr>
    <tr>
        <td rowspan="3">3 most weight combination used</td>
        <td>[5,5,5,5]</td>
        <td>[10,10]</td>
        <td>[4,4,5,4,4,4,4,4,4,4]</td>
    </tr>
    <tr>
        <td>[5,5,5,5,5]</td>
        <td>[12,12]</td>
        <td>[4,4,4,4,4,4,4,4,4,4]</td>
    </tr>
    <tr>
        <td>-</td>
        <td>[10]</td>
        <td>[3,3,4,3,3,3,3,3,3,3]</td>
    </tr>
</table>
</div>
<br/>
<br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 Voting Power Earners</th>
    </tr>
    <tr>
        <td>Address</td>
        <td>0x6e...dDec</td>
        <td>0xDc...CA7D</td>
        <td>0xB3...6B7C</td>
    </tr>
    <tr>
        <td>Bought Voting Power</td>
        <td>500</td>
        <td>200</td>
        <td>100</td>
    </tr>
    <tr>
        <td>Earned Voting Power</td>
        <td>3906</td>
        <td>1074</td>
        <td>469</td>
    </tr>
    <tr>
        <td>% Increase</td>
        <td>781.2%</td>
        <td>537%</td>
        <td>375%</td>
    </tr>
</table>
</div>
<br/>
<br/>


<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">How did the most successful voters used the vote markets?   </th>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>26</td>
        <td>25</td>
        <td>20</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>45.9%</td>
        <td>47.5%</td>
        <td>45.3%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>3.7</td>
        <td>5.8</td>
        <td>9.7</td>
    </tr>
    <tr>
        <td rowspan="3">3 most weight combination used</td>
        <td>[10,10]</td>
        <td>[5,5,5,5]</td>
        <td rowspan="3">[EACH VOTE A DIFFERENT COMBINATION]</td>
    </tr>
    <tr>
        <td>[12,12]</td>
        <td rowspan="2">[+48 DIFFERENT COMBINATIONS]</td>
    </tr>
    <tr>
        <td>[10]</td>
    </tr>
</table>
</div>


## Market predictions


<div class="sideTosideWrapper">
<div>
<img class="" src={require("../../../static/images/analytics/FV_coins21.png#bitcoinImages20").default}/>
</div>
<div>
        <strong>6</strong> out of <strong>30</strong> coins were predicted successfully.<br/>
        Successful predictions were made in rounds <strong>3,7,12,14,24,28</strong>.<br/><br/>
        
        There was no particular coin that was predicted more successfully than others. 
</div>
</div>