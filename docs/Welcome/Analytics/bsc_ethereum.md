---
sidebar_position: 1
id: bscandethereumnovember
title: Comparing Vote Markets on Binance Smart Chain and Ethereum
---
November 2021

---

**An analysis by @LIzzl Datadeo#8507**. For original analytics see [this pdf file](../../../static/images/analytics/FV_Vote-Markets-BSC_vs_ETH.pdf).<br/>
Original graphs can be seen [here](https://dune.xyz/Lizzl/Finance.vote_Vote-Markets).<br/>
Raw data: [github.com](https://github.com/datadeo/market.vote_analysis/blob/main/README.md>).

---

Since April 2021 to November 2021, users have voted 30 rounds on the Binance Smart Chain to predict the market performance of Defi tokens using a new voting mechanism called Quadratic Voting. <br/>
This report analyses the information that was compiled from those votes and compares them with the data of 30 rounds of voting on Ethereum. <br/>
The main questions answered are:

- How did users interact with the platform?
- How did voters apply Quadratic Voting?
- What makes a user a successful predictor?
- What is the predictive power of markets.vote?

## What is Quadratic Voting?

That's a voting strategy that helps balance the voting power of big players and small ones. The idea standing behind QV mechanism is to make each additional vote cost a lot more than the previous one. <br/>
It's worth to remember that in QV participants vote for or against an issue, but also express how strongly they feel about it. <br/>
The table down below depicts how cost in tokens depends on the number of votes: <br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th>Number of votes</th>
        <th>Cost in Tokens</th>
    </tr>
    <tr>
        <td>1</td>
        <td>1</td>
    </tr>
    <tr>
        <td>2</td>
        <td>4</td>
    </tr>
    <tr>
        <td>3</td>
        <td>9</td>
    </tr>
    <tr>
        <td>4</td>
        <td>16</td>
    </tr>
    <tr>
        <td>5</td>
        <td>25</td>
    </tr>
    <tr>
        <td>6</td>
        <td>36</td>
    </tr>
    <tr>
        <td>7</td>
        <td>49</td>
    </tr>
    <tr>
        <td>8</td>
        <td>64</td>
    </tr>
    <tr>
        <td>9</td>
        <td>81</td>
    </tr>
    <tr>
        <td>10</td>
        <td>100</td>
    </tr>

</table>
</div>

More on quadratic voting: [click here](/docs/Welcome/Whitepaper/tokeneconomics#quadratic-voting) or [here](/docs/Welcome/Whitepaper/votemarket#quadratic-voting)


## How does the markets.vote work?

Users are incentivized to make market predictions in a series of tournaments focussed on a basket of crypto assets. <br/>
Quadratic voting is used to generate a consensus in a perceived future market order. Users get a default voting power of 100$V. They then spend a budget of voting power to create a new order, based on their perception of token quality and future potential market performance.

Users are rewarded with a proportional share of a network-generated reward pool depending on the proportionality of their correctness.<br/>
Users can amplify their voting power beyond the starting level by demonstrating a history of correct decision-making in the markets, or by purchasing more identities.

More on markets.vote in [the whitepaper](/docs/Welcome/Whitepaper/votemarket). Check out also [markets app](https://finance.vote/#/vote/how-it-works).

## Voter Activity

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <td>Chain</td>
        <th>BSC</th>
        <th>ETH</th>
    </tr>
    <tr>
        <td>Total voters</td>
        <td>151</td>
        <td>248</td>
    </tr>
    <tr>
        <td>Total votes</td>
        <td>7994</td>
        <td>840</td>
    </tr>
    <tr>
        <td>Total voterIDs</td>
        <td>654</td>
        <td>318</td>
    </tr>
    <tr>
        <td>Average votes per round</td>
        <td>265</td>
        <td>28</td>
    </tr>
    <tr>
        <td>Average user would mint</td>
        <td>4 voterIDs</td>
        <td>2 voterIDs</td>
    </tr>

</table>
</div>


For up to date statistics visit [Lizzl page on Dune Analytics](https://dune.xyz/Lizzl/Finance.vote_Vote-Markets).

### Adoption Curve
#### ETH




<iframe id="ETHadoption"
        title="ETHadoption"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/211090/396383/a4f2bbd7-e77c-4f08-87be-598ccec9eef5">
</iframe>

#### BSC

<iframe id="BSCadoption"
        title="BSCadoption"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/209061/392154/b9e38e72-3a09-471f-926f-8be2acd5a0ff">
    </iframe>


### Voter Turnout
#### ETH

 <iframe id="ETHturnout"
        title="ETHturnout"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/211099/396410/549f1508-4fba-4d5b-9654-8938cccf93f6">
    </iframe>

|

#### BSC
<iframe id="BSCturnout"
        title="BSCturnout"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/200999/375115/72b257bb-fad5-4f2f-a13d-de070564c2ae">
    </iframe>


### VoterIDs per Wallet
#### ETH

 <iframe id="ETHwalletIDs"
        title="ETHwalletIDs"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/212234/398687/037358a0-87d6-4bc5-95ff-3d8fb3008ce4">
    </iframe>


#### BSC

<iframe id="BSCwalletIDs"
        title="BSCwalletIDs"
        width="573"
        height="380"
        scrolling="no"
        src="https://dune.xyz/embeds/212059/398316/23c71749-ea02-4714-a3c0-c49d69a75b13">
    </iframe>

|

### Distribution of Voting Power and VoterIDs
#### ETH


<p align="center">

![Distribution ETH](../../../static/images/analytics/distributionETH.jpg)
</p>


#### BSC

<p align="center">

![Distribution BSC](../../../static/images/analytics/distributionBSC.jpg)
</p>



## How did voters apply QV?
### ETH

<div class="sideTosideWrapper">
<div>
<img class="" src={require("../../../static/images/analytics/handsETH.png#bitcoinImages700px").default}/>
</div>
<div>
    On average, voters used <strong>58%</strong> of their voting power per vote on ETH. <br/>
    <strong>100, 200 & 99</strong> were the three most used amounts of voting power. <br/>
    On average, voters made <strong>2</strong> choices per vote on ETH: <br/>
    With <strong>10, 1, 2</strong> being the most used coin choices. <br/>
</div>
</div>


<p align="center">

![Bitcoin](../../../static/images/analytics/contributionETH.jpg)
</p>


### BSC
<div class="sideTosideWrapper">
<div>
<img class="" src={require("../../../static/images/analytics/handsBSC.png#bitcoinImages20").default}/>
</div>
<div>
    On average, voters used <strong>40%</strong> of their voting power per vote on BSC. <br/>
    <strong>100, 99 & 97</strong> were the three most used amounts of voting power. <br/>
    On average, voters made <strong>10</strong> choices per vote on BSC, choosing all ten coins in one vote.
</div>
</div>

<p align="center">

![Bitcoin](../../../static/images/analytics/contributionBSC.jpg)
</p>




## Coin clairvoyants

### Top 3 wallets with most Voting Power

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 wallets with most Voting Power on BSC chain</th>
    </tr>
    <tr>
        <td>Address</td>
        <td>0x34...39EA</td>
        <td>0xE3...D58f</td>
        <td>0x8E...734b</td>
    </tr>
    <tr>
        <td>Voting Power</td>
        <td>45 515</td>
        <td>36 498</td>
        <td>30 994</td>
    </tr>
    <tr>
        <td># of voterID</td>
        <td>50</td>
        <td>32</td>
        <td>27</td>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>30</td>
        <td>28</td>
        <td>26</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>36.9%</td>
        <td>33.3%</td>
        <td>29.9%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>9.6</td>
        <td>9.6</td>
        <td>9.8</td>
    </tr>
    <tr>
        <td rowspan="3">3 most weight combination used</td>
        <td>[3,3,4,1,4,4,2,2,3,4]</td>
        <td>[3,3,3,2,4,3,3,3,3,4]</td>
        <td>[5,5,5,5,5,5,3,4,3,4]</td>
    </tr>
    <tr>
        <td>[7,7,7,8,8,6,5,6,7]</td>
        <td>[4,3,3,2,3,3,3,3,4,3]</td>
        <td>[10,8,10,7,10,10,8,8,8,8]</td>
    </tr>
    <tr>
        <td>[9,9,9,9,9,9,3,9,9]</td>
        <td>[4,4,5,5,4,4,3,6,5,4]</td>
        <td>[4,4,4,5,4,4,3,3,3,3]</td>
    </tr>
</table>
</div>
<br/>
<br/>



<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 wallets with most Voting Power on ETH chain</th>
    </tr>
    <tr>
        <td>Address</td>
        <td>0x34...39EA</td>
        <td>0x6e…dDec</td>
        <td>0x67…e640</td>
    </tr>
    <tr>
        <td>Voting Power</td>
        <td>5400</td>
        <td>4406</td>
        <td>3501</td>
    </tr>
    <tr>
        <td># of voterID</td>
        <td>50</td>
        <td>5</td>
        <td>15</td>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>3</td>
        <td>26</td>
        <td>16</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>95.9%</td>
        <td>45.9%</td>
        <td>61.3%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>4.1</td>
        <td>3.7</td>
        <td>9.9</td>
    </tr>
    <tr>
        <td rowspan="3">3 most weight combination used</td>
        <td>[5,5,5,5]</td>
        <td>[10,10]</td>
        <td>[4,4,5,4,4,4,4,4,4,4]</td>
    </tr>
    <tr>
        <td>[5,5,5,5,5]</td>
        <td>[12,12]</td>
        <td>[4,4,4,4,4,4,4,4,4,4]</td>
    </tr>
    <tr>
        <td>-</td>
        <td>[10]</td>
        <td>[3,3,4,3,3,3,3,3,3,3]</td>
    </tr>
</table>
</div>

---

### Top 3 Voting Power Earners on BSC

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 Voting Power Earners on BSC</th>
    </tr>
    <tr>
        <td>Address</td>
        <td>0x34...39EA</td>
        <td>0xE3...D58f</td>
        <td>0x8E...734b</td>
    </tr>
    <tr>
        <td>Bought Voting Power</td>
        <td>5000</td>
        <td>3200</td>
        <td>2700</td>
    </tr>
    <tr>
        <td>Earned Voting Power</td>
        <td>40 515</td>
        <td>33 298</td>
        <td>28 294</td>
    </tr>
    <tr>
        <td>% Increase</td>
        <td>8.1%</td>
        <td>10.4%</td>
        <td>10.4%</td>
    </tr>
</table>
</div>

<br/>
<br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">On BSC the Top 3 Earners are the same wallets with most Voting Power</th>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>30</td>
        <td>28</td>
        <td>26</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>36.9%</td>
        <td>33.3%</td>
        <td>29.9%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>9.6</td>
        <td>9.6</td>
        <td>9.8</td>
    </tr>
    <tr>
        <td rowspan="3">3 most weight combination used</td>
        <td>[3,3,4,1,4,4,2,2,3,4]</td>
        <td>[3,3,3,2,4,3,3,3,3,4]</td>
        <td>[5,5,5,5,5,5,3,4,3,4]</td>
    </tr>
    <tr>
        <td>[7,7,7,8,8,6,5,6,7]</td>
        <td>[4,3,3,2,3,3,3,3,4,3]</td>
        <td>[10,8,10,7,10,10,8,8,8,8]</td>
    </tr>
    <tr>
        <td>[9,9,9,9,9,9,3,9,9]</td>
        <td>[4,4,5,5,4,4,3,6,5,4]</td>
        <td>[4,4,4,5,4,4,3,3,3,3]</td>
    </tr>
</table>
</div>


---

### Top 3 Voting Power Earners on ETH

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 Voting Power Earners on ETH</th>
    </tr>
    <tr>
        <td>Address</td>
        <td>0x6e…dDec</td>
        <td>0xDc…CA7D</td>
        <td>0xB3…6B7C</td>
    </tr>
    <tr>
        <td>Bought Voting Power</td>
        <td>500</td>
        <td>200</td>
        <td>100</td>
    </tr>
    <tr>
        <td>Earned Voting Power</td>
        <td>3906</td>
        <td>1074</td>
        <td>469</td>
    </tr>
    <tr>
        <td>% Increase</td>
        <td>781.2%</td>
        <td>537%</td>
        <td>375%</td>
    </tr>
</table>
</div>

<br/>
<br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">How did the most successful voters used the vote markets?</th>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>26</td>
        <td>25</td>
        <td>20</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>45.9%</td>
        <td>47.5%</td>
        <td>45.3%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>3.7</td>
        <td>5.8</td>
        <td>9.7</td>
    </tr>
    <tr>
        <td rowspan="3">3 most weight combination used</td>
        <td>[10,10]</td>
        <td>[5,5,5,5]</td>
        <td rowspan="3">[EACH VOTE A DIFFERENT COMBINATION]</td>
    </tr>
    <tr>
        <td>[12,12]</td>
        <td rowspan="2">[+48 DIFFERENT COMBINATIONS]</td>
    </tr>
    <tr>
        <td>[10]</td>
    </tr>

</table>
</div>

---
### Top 3 VoterIDs BSC

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 VoterIDs BSC</th>
    </tr>
    <tr>
        <td>VoterID</td>
        <td>54</td>
        <td>61</td>
        <td>121</td>
    </tr>
    <tr>
        <td>Voter Power</td>
        <td>3475</td>
        <td>2903</td>
        <td>2628</td>
    </tr>

</table>
</div>

<br/>
<br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">How did the most successful VoterIDs used the vote markets?</th>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>25</td>
        <td>22</td>
        <td>27</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>21.5%</td>
        <td>20.6%</td>
        <td>26.9%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>4.3</td>
        <td>4</td>
        <td>9.3</td>
    </tr>
    <tr>
        <td>3 most weight combination used</td>
        <td colspan="4">[EACH VOTE A DIFFERENT COMBINATION]</td>
    </tr>


</table>
</div>

---
### Top 3 VoterIDs ETH

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">Top 3 VoterIDs ETH</th>
    </tr>
    <tr>
        <td>VoterID</td>
        <td>195</td>
        <td>1</td>
        <td>197</td>
    </tr>
    <tr>
        <td>Voter Power</td>
        <td>2021</td>
        <td>1226</td>
        <td>1056</td>
    </tr>

</table>
</div>

<br/>
<br/>

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th colspan="4">How did the most successful VoterIDs used the vote markets?</th>
    </tr>
    <tr>
        <td># of rounds participated</td>
        <td>23</td>
        <td>27</td>
        <td>23</td>
    </tr>
    <tr>
        <td>% of voting power used</td>
        <td>39%</td>
        <td>28.4%</td>
        <td>42.7%</td>
    </tr>
    <tr>
        <td>AVG # of choices</td>
        <td>3.6</td>
        <td>7.6</td>
        <td>3.6</td>
    </tr>
    <tr>
        <td>3 most weight combination used</td>
        <td colspan="4">[EACH VOTE A DIFFERENT COMBINATION]</td>
    </tr>


</table>
</div>

## Market predictions

**21** out of **30** coins were predicted successfully on **BSC**. <br/>
Successful predictions were made in all rounds but **3,5,6,10,12,14,16,23,29**. <br/>
**BUSD** was the token that was predicted most successfully. <br/>

**6** out of **30** coins were predicted successfully on **ETH**.<br/>
Successful predictions were made in rounds **3,7,12,14,24,28**.<br/>
There was no particular coin that was predicted more successfully than others.<br/>

<div class="sideTosideWrapper">
<div>
BSC
<img class="" src={require("../../../static/images/analytics/BSCgraph.png#bitcoinImages20").default}/>
</div>
<div>
ETH
<img class="" src={require("../../../static/images/analytics/ETHgraph.png#bitcoinImages20").default}/> 
</div>
</div>