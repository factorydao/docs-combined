---
sidebar_position: 2
id: signal
title: Finding Signal in markets.vote
---

**An analysis of the first year of markets.vote by @ShiftRunStop#8542** [source](https://dry-forest-0623.on.fleek.co/181121_markets_vote_analysis.html)

---
**markets.vote** has been running since *November 2020* on Ethereum and since *April 2021* on Binance Smart Chain. 

On each chain, finance.vote ID holders use quadratic voting to choose which token(s) they think will perform best over the coming week. At the end of the market period, 100,000 FVT is distributed amongst all those who chose the winning token, proportional to the number of votes they assigned the winner. 

Winning voters will also receive more voting power for forthcoming vote markets. The list of coins to vote on is different on each chain.

On Ethereum the list is dominated by the big players in DeFi as well as the OG cryptocurrency, Bitcoin, and a USD stablecoin:
- Chainlink (LINK)
- Wrapped Bitcoin (WBTC)
- Dai (DAI)
- Uniswap (UNI)
- Maker (MKR)
- Compound (COMP)
- UMA (UMA)
- yearn.finance (YFI)
- Synthetix Network Token (SNX)
- Ren (REN)

The list of coins on Binance Smart Chain is a more general mix of high cap cryptocurrencies and a USD stablecoin:
- Cardano (ADA)
- Bitcoin Cash (BCH)
- Binance Coin (BNB)
- Binance USD (BUSD)
- Polkadot (DOT)
- Ethereum (ETH)
- Filecoin (FIL)
- Litecoin (LTC)
- Tron (TRX)
- Ripple (XRP)

## Predicting the Market
The cryptocurrency market is notoriously volatile and unpredictable. It is clearly not possible to perfectly predict the future. But with the combined consideration, market analysis and intuition of hundreds of crypto degens will it be possible to predict the market more profitably than relying on your own strategy?

Beyond the fact that the crypto market is fundamentally difficult to predict, a number of other factors came into play that may have reduced the quality of prediction signal coming out of markets.vote.

Factors with a negative impact on predict market:

- **Mass hedged voting** <br/>
    
    Despite attempts at sybil resistance to stop users minting many IDs and voting multiple times, there is clear evidence that this happened. It happened most on Binance Smart Chain where the parameters affecting the base price and decay rate of minting IDs on that chain failed to provide sufficient sybil resistance. This, coupled with the low cost of voting on BSC meant that many users heavily hedged their votes across all tokens. As there was no penalty for guessing incorrectly, there was nothing to lose from voting many times in order to win a larger share of the FVT award each week. This phenomenon also occurs on Ethereum but to a lesser extent. On the Ethereum chain, the sybil resistance design worked more effectively initially due to a decay rate which kept the price of multiple IDs in quick succession unaffordable. Also, the art attached to Ethereum IDs for a period in early 2021 increased the desirability of the NFTs and kept the price to a level that made it unviable economically to mint multiple IDs purely with the intention of earning rewards on markets.vote. Finally the high price of gas on the Ethereum chain meant that voting en masse was at times excruciatingly expensive and meant it did not make financial sense to vote many times as the rewards may not have covered the gas fees.

- **People look back not forward** <br/>
    The data shows that voters continually voted highly for tokens that were doing well that week. Presumably with the confidence that they would continue to do well the following week. While this was sometimes true, history proves that it was not usually the case - tokens in fact tended to have short term spikes in price and were often corrected after a short term rise.

- **Lots of casual voting** <br/>
    It's probably fair to say that the majority of people who voted did so having not done a great deal of research or consideration into what tokens may actually perform well the following week. Besides gas fees (which were often considered on the Ethereum network) there was nothing at stake to lose for incorrect predictions. This resulted in lots of votes being placed with little to no consideration for how well that token may actually perform over the coming week.

## Potential Strategies
A number of investment strategies that could have been adopted based on the week's prediction have been chosen for the purpose of this analysis. All of the historic voting data has been extracted from the contract as well as all of the token prices at the start and end of each round's market period. Using this data it has been possible to play out each strategy to see how much money would have been made (or lost) by adopting each.

The strategies chosen for this exercise were as follows. Each strategy begins with $1000.

1. **Baseline** <br/>
    Invest $100 into each token after voting for round 1 closes. HODL.
2. **Prediction weighted** <br/>
    For those that have absolute trust in their fellow voters. After the voting for each round closes distribute your accrued capital proportionally across all of the tokens according to the number of votes they received in the prediction market.
3. **All in** <br/>
    After the voting for each round closes, the entire accrued capital is moved into the token that received the highest number of votes.
4. **Top four** <br/>
    After the voting for each round closes, the accrued capital is divided evenly amongst the four tokens that have received the highest number of votes.
5. **All in pessimist** <br/>
    For those that believe that the group will consistently fail to pick out the token that will perform best the following week. After the voting for each round closes, the entire accrued capital is moved into the token that received the lowest number of votes.
6. **Bottom four** <br/>
    As strategy 5, but after the voting for each round closes, the accrued capital is divided evenly amongst the four tokens that have received the lowest number of votes.
7. **Top; bottom** <br/>
    For those that think the group is likely to choose the best token for coming week or the worst, and not much inbetween. After the voting for each round closes, the accrued capital is divided evenly between the token that received the highest number of votes and the token that received the lowest number of votes.
8. **Top 2; Bottom 2** <br/>
    As strategy 8 but a bit more hedgy. After the voting for each round closes, the accrued capital is divided evenly amongst the two tokens that have received the highest number of votes as well as the two tokens that have received the lowest number of votes: 25% to each token.
9. **High cap maximalist** <br/>
    Invest the full $1000 into the highest market cap coin in the market and hodl (BTC on Ethereum markets.vote; ETH on BSC markets.vote)
#. **BONUS STRATEGY: Crypto Hamster** <br/>
    Inspired by [this news story](https://www.bbc.com/news/technology-58707641) from September 2021, this strategy simulates what would have happened if you'd allowed your pet hamster to choose which of the 10 tokens you moved all of your capital into at the start of each round. You can respawn the hamster with a new set of choices by clicking the button below each graph in the analysis for each chain below.

:::note
Where applicable, any trading fees that would need to be paid each week when exchanging from one token to another have not been taken into account in this analysis. On the occasions where multiple coins received the same number of votes the capital was evenly divided between them both.
:::

## BSC markets.vote data
<iframe id="inlineFrameExample"
        title="Inline Frame Example"
        width="700"
        height="654"
        scrolling="no"
        src="https://dry-forest-0623.on.fleek.co/181121_markets_vote_analysis.html#chartBSC">
    </iframe>
<br/>
<br/>

It's fairly clear from the similarity of the lines for all strategies on the chart above that **no clear signal can be drawn from the historic markets.vote data on the Binance Smart Chain.** This is almost certainly because mass hedged voting by a relatively small number of voters using multiple IDs has introduced too much noise to the data. <br/>
It is notable, however, that despite the apparent lack of a clear signal in this data the strategy that would have earned the investor **the most money would have been to go all-in each week on the token** that received the most votes from the markets.vote users.

On the original site there is a [button](https://dry-forest-0623.on.fleek.co/181121_markets_vote_analysis.html#addHamsterBsc) that allows to respawn the BSC Crypto Hamster numerous times. Doing that confirms that following no single strategy based on BSC markets.vote data would have produced a markedly different result than getting your pet hamster to do your investing for you.

## ETH markets.vote data
<iframe id="inlineFrameExample"
        title="Inline Frame Example"
        width="700"
        height="654"
        scrolling="no"
        src="https://dry-forest-0623.on.fleek.co/181121_markets_vote_analysis.html#chartETH">
    </iframe>
<br/>
<br/>

In contrast to the data produced by markets.vote on BSC, on Ethereum there are distinct differences in outcome between the different strategies that could have been employed. One remarkable thing is that markets.vote voters consistently failed to choose the token that would perform best the following week. 
Furthermore, they often chose the token that would perform the worst. This is reflected by the fact that after 52 rounds of voting, if after each week an investor had moved their entire capital to the token predicted to perform best they would end up with **only $852 from their initial $1000**.

Even more remarkable is that the strategy that performed best over 52 weeks was for an investor to move their entire capital each week to **the token that had received the fewest votes**. If that strategy was consistently adopted the initial investment of $1000 would have become $10,369 after 52 weeks. This is a 12× better return than investing in the most voted for token each week.

Mashing the ["Respawn ETH Crypto Hamster"](https://dry-forest-0623.on.fleek.co/181121_markets_vote_analysis.html#addHamsterEth) button quickly highlights that only very rarely would a near brain-dead rodent have made more money than if you had consistently invested in the very tokens that the markets.vote users told you not to. Conversely, only very rarely would little Hammy have done more damage to your initial $1000 investment than if you had consistently moved your investment to the token with the most votes.

Just so you know what you could have won, **if the markets.vote users had perfectly predicted the market each week** and you had consistently moved your entire capital each week to follow their predictions, **your initial investment of $1000 would have become $267.5 million after a year**.


## References
- Binance Smart Chain markets.vote historic voting data extracted from [the markets.vote contract using bscscan](https://bscscan.com/address/0xa6bb733a61f2f4f09090781cccd80929c9234b3f).
- Ethereum markets.vote historic voting data was extracted from [the markets.vote contract using etherscan](https://etherscan.io/address/0xea6556e350cd0c61452a26ab34e69ebf6f1808ba).
- Historic token prices for the start and end of each market period for both blockchains were obtained from [cryptowat.ch](https://cryptowat.ch/) and their [cryptofinance](https://cryptofinance.ai/) plugin for Google Sheets.
- If you wish to analyse this data fully and perhaps come up with some of your own strategies to play out, here is the [full data on Google Sheets](https://docs.google.com/spreadsheets/d/1YldctjHWdCwIKlZH6g2eZ6X6m7skNIOwoOVxmDYpHAI/edit#gid=2019966782).
- Interactive charts powered by [ApexCharts](https://apexcharts.com/).