---
sidebar_position: 1
id: intro
title: FactoryDAO
---

FactoryDAO leads the DAO wave with an extensive suite of dApps for building sustainable, decentralised organisations. Building no-code, modular infrastructure, allowing anyone to launch a DAO.

FactoryDAO is a decentralised autonomous organisation (DAO) that builds software (smart contracts and browser interfaces). This software enables users to easily generate a token economy and govern themselves as a DAO.
We believe in Truth, Knowledge, and Imagination and will seek democratic outcomes to becoming an important digital creative institution in the future.

### Our dApp suite:
<br/>
<table class="tablech">
  <tr class="tablech_tr">
    <td class="slimmm"><span class="iconDapp">

![Influence Logo](../../static/images/logos/influence_icon.png)
</span></td>

<td><a href="https://influence.factorydao.org/"> Influence dApp</a></td>
    <td><a href='../suite-docs/influence/welcome'> Docs</a></td>

</tr>

  <tr class="tablech_tr">
    <td class="slimmm"><span class="iconDapp">

![Bank Logo](../../static/images/logos/bank_icon_dark.png)
</span></td>

<td><a href='https://bank.factorydao.org/#/'> Bank dApp</a></td>
    <td><a href='../suite-docs/bank/welcome'> Docs</a></td>

</tr>

  <tr class="tablech_tr">
    <td class="slimmm"><span class="iconDapp">

![Launch Logo](../../static/images/logos/launch_icon.png)
</span></td>

<td><a href="https://launch.factorydao.org/"> Launch dApp</a></td>
    <td><a href='../suite-docs/launch/welcome'> Docs</a></td>

</tr>

  <tr class="tablech_tr">
    <td class="slimmm"><span class="iconDapp">

![Markets Logo](../../static/images/logos/markets_icon.png)
</span></td>

<td><a href="https://finance.vote/"> Markets dApp</a></td>
    <td><a href='../suite-docs/markets/welcome'> Docs</a></td>

</tr>

  <tr class="tablech_tr">
    <td class="slimmm"><span class="iconDapp">

![Mint Logo](../../static/images/logos/mint_icon.png)
</span></td>

<td><a href="https://mint.factorydao.org/"> Mint dApp</a></td>
    <td><a href='../suite-docs/mint/welcome'> Docs</a></td>

</tr>

  <tr class="tablech_tr">
    <td class="slimmm"><span class="iconDapp">

![Yield Logo](../../static/images/logos/yield_icon.png)
</span></td>

<td><a href="https://yield.factorydao.org/#/"> Yield dApp</a></td>
    <td><a href='../suite-docs/yield/welcome'> Docs</a></td>

</tr>
</table>
