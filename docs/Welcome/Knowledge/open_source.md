---
sidebar_position: 6
id: opensource
title: Open-Source
---
Open-source is a wide topic that we could speak about for days. Doing so probably wouldn’t win us any 'life of the party' awards but that’s the cost of getting too deep into any topic.<br/>
We'll try to keep it simple here.

You've probably heard the term "open-source" a million times, but do you really know what it means?<br/>
**Open-source** can be a source code, design, recipe, etc. that can be further used or improved upon without any legal consequences from the original author. Any user can download, copy, modify, and use it commercially. You literally can do anything with it (but you know, be good!). 

The decentralized software-development model, which we are concerned about in the crypto world, is based on the concept of open-source. That means, it is not only a "take-and-use-for-your-own-purposes" kind of thing but also a "collaborative improvement over the product" kind of thing. 

The Linux system is the most ubiquitous example of an open-source product that has been improved upon by many.  In 2017, there were roughly 15,600 developers working to improve Linux according to this [report](https://www.linuxfoundation.org/resources/publications/state-of-linux-kernel-development-2017) and the development still continues. 

To achieve such **a great community distribution** to a project, a project itself has to be worth the effort and it should bring joy and pride to the developers themselves--all of which Linux does perfectly!


Some important links before we go further:
* Open-source main rules: https://opensource.org/osd
* Open-source Initiative: https://opensource.org/

Want some examples?
--------------------
.. linuximage:: ../../images/logos/linux.png
    :height: 20px
    :target: https://ubuntu.com/desktop/developers

.. blenderimage:: ../../images/logos/blender.svg
    :width: 60px
    :target: https://www.blender.org

.. arduinoimage:: ../../images/logos/arduino.png
    :height: 20px
    :target: https://www.arduino.cc/

.. CCimage:: ../../images/logos/cc.svg
    :width: 20px
    :target: https://creativecommons.org/

.. openemrimage:: ../../images/logos/openemr.svg
    :width: 60px
    :target: https://www.open-emr.org/

.. osddimage:: ../../images/logos/osdd.gif
    :width: 60px
    :target: http://www.osdd.net/

Here you go! These are some great examples of open-sourced projects: 

<span class="iconDapp">

- ![Linux Logo](../../../static/images/logos/linux.png) [Linux](https://ubuntu.com/desktop/developers)

</span>

<span class="iconDapp">

- ![Blender Logo](../../../static/images/logos/blender.svg) [Blender](https://www.blender.org/)

</span>

<span class="iconDapp">

- ![Arduino Logo](../../../static/images/logos/arduino.png) [Arduino](https://www.arduino.cc/)

</span>

- [Open-source colas](https://web.archive.org/web/20010218075323/http://www.opencola.com/download/3_softdrink/formula.shtml) (out of that recipe, [Cube cola](https://cube-cola.org/) arose)
- [Free Beer](http://freebeer.org/blog/)

<span class="iconDapp">

- ![CC Logo](../../../static/images/logos/cc.svg) [Creative Commons](https://creativecommons.org/)

</span>

<span class="iconDapp">

- ![OpenEMR Logo](../../../static/images/logos/openemr.svg) [OpenEMR](https://www.open-emr.org/)

</span>

<span class="iconDapp">

- ![Open Source Drug Discovery Logo](../../../static/images/logos/osdd.gif) [Open Source Drug Discovery](http://www.osdd.net/)

</span>

- [Hyperloop](https://www.tesla.com/sites/default/files/blog_images/hyperloop-alpha.pdf)
- [WikiHouse](https://www.wikihouse.cc/)
- ...and countless more :)


## Is open-source secure?
Free, open, available to everyone, collaborative improvement - has this all triggered your alarm bells? <br/>
Unfortunately, we do still live in a world where not everyone's intent is noble.<br/>
Since open-source has a rule of 'No Discrimination Against Persons or Groups', this means that even bad actors have access to the source code. 

The question is **do we have security measures against such nefarious actors?**  ...and the answer is: **to some extent**. <br/>
For starters, it's not that easy to put a malicious piece of code into someone's repository. One would have to have access to the repository first and then pass the code review.

Though, someone could attempt to run an attack by adding a malware package to the download page. <br/>
Righteous developers likely haven't anticipated every possible security issue and bad actors could find an open gate into a system to do some monkey business.

Methods are clever and numerous, making it impossible to describe them all. That's why users should always aware of potential danger and avoid perilous actions:

- Always download source code from trusted sites and make sure you're downloading the official release
- Check to see that the online repository where the source code is stored has security support (like code-scan bots, autofixes etc.)
- Support projects that have earned your trust and try to find ones where the authors are not anonymous and have a solid community

Also, it is very important, though hard to check, that the developers review the code in a proper way. When it comes to open-source and working with volunteers, there shouldn't be such thing as "just taking a look" into the code. 

**Code should always be thoroughly checked whether it acts as described or not.** Otherwise, a backdoor could be overlooked and that’s when you've got a huge problem.


## Why do we even need open-source?
It's so much easier to scratch an itchy spot yourself, than trying to explain where it is to someone else and having them assist. Open-source works in a similar way too, where a user who needs something or has an idea to improve something, could simply do it themselves. This has the added benefit of helping others in the community who could benefit from your fix or improvement. 

Contribution to a project you use and need confers many benefits such as:   
- Better working project
- Greater user satisfaction 
- Better product understanding
- Larger commitment to the product, making it last longer
- And a stronger incentive for other users and contributors to use or commit to the product

Even more important, is that improvements are not only to the functionality of a product but also to its security. As discussed before, there are a variety of possible attacks, so users' commits are crucial.<br/>
Considering the cryptoworld exclusively, open-source brings these benefits:
- Helps to build decentralization which allows cryptocurrencies to be independent from corporate interest
- Allows for self-checking the code, so you know exactly where your money is going leading to greater trust between participants

And when we put all this together, we get a whole ecosystem with committed users who help build decentralization, correct security issues, use the products themselves, and encourage others to do so as well. <br/>
Developers in the cryptoworld who build new projects based on a source code help to "populate it" making it more fit, useful, and friendly to all types of users down to the novice or 'noob'. More can join and find a product that is right for them.<br/>
It's happening right now as we write--crypto-based ecosystems are rapidly developing, spreading, and becoming more and more important in the real world every day with the potential of becoming the future of global finance. 

## Conclusion
Open-source is important in the cryptoworld because it allows crypto-ecosystem to futher develop and to become more secure, recognizable, and useful. The result of open-source software development are highly resilient codes made by users to serve users. 
