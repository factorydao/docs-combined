---
sidebar_position: 2
id: blockchain
title: Blockchain
---

## What is it?
------------
Blockchain is a record-keeping technology standing--among others--behind cryptocurrency networks. To understand that better, we can say that blockchain is a specific type of database storing data in blocks which are then chained together. In this case, 'block' means a group of information.

The cryptocurrency world is not the only place where blockchain is being used though. It is definitely the most obvious use case these days, but blockchain is also being used with legal contracts, product inventories, and any other place where a trustworthy, secure and fast system may be needed.

For example:

- medical data
- personal identity security
- voting mechanisms
- real estate processing platforms
- and many more


## Types of blockchain networks
Over the years, blockchain has been evolving and new user needs have come up. Depending on how a blockchain is configured, there are several business cases it can fulfill.

### Public blockchain
In a public blockchain, anyone is free to join and participate in the core activities of the blockchain network. Anyone can read, write, and audit the ongoing activities on the public blockchain network, which helps a public blockchain maintain its self-governed nature. Public blockchains can be fully decentralized and democratized, and their transactions can stay authority-free.

### Private blockchain
Private blockchain is useful whenever there is a need to verify participants, allow entrance only by authentic and verified invitations, and to decide who can make transactions or authenticate blockchain transactions and changes.

The ledger in this solution is a secure database based on cryptographic concepts. It is not decentralized though since it is closed; only allowed users can look into it.<br/>
Within a private blockchain, only the owner or the operator has the right to override, edit, or delete the necessary entries on the blockchain as required. Also, the consensus protocol works differently (for more information see the 'How is it secured?' section below); its execution is dependent on specific users. Further, mining rights and rewards are decided in advance.<br/>
This type of blockchain is most commonly used in private business.


### Permissioned blockchain
Permissioned blockchain is a mixed type of public and private blockchain. It has many customization options. For example, there could be a customized blockchain where anyone could join but only a certain number of activities would be allowed to be performed by a user.<br/>
This is also known as semi-private, consortium, or federated blockchain. The consensus process within this type of network is controlled by a pre-selected set of nodes.

## How does it work?
Regardless of the type of blockchain network used, there is always the possibility of making transactions on it. No matter what type of transaction it is, once it is made it gets recorded and its authenticity must be verified by the blockchain network. This is done by computers within the blockchain that confirm that the details of the transaction are correct. After successful validation, a transaction is added to a blockchain block.

Imagine for example you're working in a dairy and your job is to fill and cap bottles of milk. Whenever a bottle is filled up, you need to cap it and put it in the box with the other filled up bottles. 

Similarly, a block has a defined capacity too. Whenever it fills up, it gets closed and chained to the previous block. In this way, every entry and all information is permanently saved to the block and stored. Consequently, we can build a transparent database and do not have to worry about any unwanted data changes since that would be impossible without leaving a trace (see below for further explanation about possible inconsistency within blocks and possible changes within a chain).

When a new block is added to the chain it gets a timestamp, a unique self-hash, and a unique hash of the previous block. Timestamps are responsible for chaining blocks together. As blocks are set in a chronological order on the timeline of a blockchain life, timestamps become a specific and immutable connection between blocks. Hash, in simple words, is just a mathematical translation of the content inside a block saved in the form of a string of numbers and letters.

## How is it computed?
Imagine there is a very long and complex mathematical equation. You have to work on one part of the equation at once and solve that step by step till the end. To save some time, you could split the equation up and give out parts of it to several people to solve simultaneously. The only thing left would be to sum up all of the resolved parts together. Voila, you've saved some energy and time.
 
Transactions on the blockchain network are computed in a similar fashion. Every block is filled with data represented by code. That code needs to be computed and solved just like the
mathematical equation. Thus, splitting all of the computation ​into several machines is more effective than using just one computing engine. That is of course possible, but less effective.

In the real world of blockchain, these kinds of computations are very numerous and spread across a network of nodes. The amount of transactions happening across a blockchain are so enormous that every node regularly computes more than one equation for more than one transaction. 


## Transparency
What exactly does it mean when we say a database is ‘transparent’? Many databases within the world of blockchain are set to public, which means that anyone can join the network and view all of the transactions on that network. To do this, a user must be a part of the blockchain network and use a blockchain explorer that allows them to see all transactions. Within a blockchain, there is no way of doing anything to the blockchain without it being recorded. At the same time, any sensitive data such as identifying information is encrypted and safe. They are still shown and called a 'public key'.

For example, a virtual wallet address is just encrypted information which is unreadable to humans and useless to a system that does not have a decryption key.

## Where is it stored?
Blockchain like every other database-like structure needs to be stored. Blockchains that represent small product inventories can be stored on a group of computers existing in one place since they do not have to store thousands of transactions and handle  large network traffic. 

With bigger systems such as those for cryptocurrency, it is hard to store all of the data in one place even with a large amount of computers. To solve this problem, blockchain is spread all over the world and stored on a huge number of machines, which in blockchain lingo are called 'nodes'. That is why a blockchain storage system is commonly known as a decentralized storage system.

Each node within a network has a full record of the data that has been stored on the blockchain since its inception. But, what happens if one of the nodes has an error in its data you may ask? Luckily, the blockchain can use thousands of other nodes as a reference point to get the data correct again. This is yet another proof of the irreversibility and immutability of a blockchain system because no node can alter any data. Although, even if one tries, other nodes cross-referencing eachother would easily pinpoint the node with the incorrect information.

## How is it secured?
New blocks are always stored linearly and chronologically. Every block has a certain position on the chain called 'height'.

It was mentioned above that chains are immutable but this is not an entirely accurate statement. Changes are possible only if a majority in a chain agree on the change. This is called 'reaching consensus' which is used for the purpose of security.

Why is reaching consensus so important? As pointed out before, when a new block is added to the chain it gets a timestamp, a unique self-hash, and a unique hash of the previous block.  When any changes are being made, new hashes and timestamps are created. Without a reaching consensus method, changes could easily be overlooked.  This could lead to conflicts between blocks or at worse, malicious changes being added to a blockchain. 

Does this mean that an attack on the blockchain is impossible--an attack where someone takes majority control of the chain and adds inaccurate entries for gain? No. There is a possible scenario where a hacker or hackers could overpower the chain by gaining majority control of the chain. However, this kind of attack is incredibly unprofitable since handling the majority of a chain would be incredibly expensive, rendering an attack virtually pointless. In this way, participating in the network is far more economically viable than trying to attack or overpower it. 


