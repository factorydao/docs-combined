---
sidebar_position: 1
---

# Welcome to influence

Influence is designed for token weighted decentralised content curation and rough consensus formation around anything from tuning monetary policy parameters to meme markets. This is where DAO members deliberate and decide.


## What is influence for?

