---
sidebar_position: 4
id: strategies
title: Strategies Explained
---
#### How to pick a strategy?
##### For Admins and members
---

## Strategies
Influence supports various contracts, token standards and chains. Implemented different calculating methods called strategies help to handle such diversity and cover most user needs.

Strategies used in the app are built out of params. Those 6 of them (listed in the next section) create 6 basic strategies:

<div class="tableWrapper">
<table class="tableAnalytics">
    <tr>
        <th>Strategy</th>
        <th>Base</th>
        <th>Voting</th>
        <th>Power Calculating</th>
    </tr>
    <tr>
        <td>Token weighted quadratic voting</td>
        <td>token-based</td>
        <td>quadratic</td>
        <td>not-aggregated</td>
    </tr>
    <tr>
        <td>Token weighted binary voting</td>
        <td>token-based</td>
        <td>binary</td>
        <td>not-aggregated</td>
    </tr>
    <tr>
        <td>Quadratic voting</td>
        <td>nft-based</td>
        <td>quadratic</td>
        <td>not-aggregated</td>
    </tr>
    <tr>
        <td>Binary voting</td>
        <td>nft-based</td>
        <td>binary</td>
        <td>not-aggregated</td>
    </tr>
    <tr>
        <td>Aggregated quadratic voting</td>
        <td>nft-based</td>
        <td>quadratic</td>
        <td>aggregated</td>
    </tr>
    <tr>
        <td>Aggregated binary voting</td>
        <td>nft-based</td>
        <td>binary</td>
        <td>aggregated</td>
    </tr>
</table>
</div>

Any other strategies are custom and require a separate approach to meet even the highest user requirements.

## Main params
1. Base:
   -  **token-based**<br/>
   takes user balance of ERC20 token and grants voting power based on that amount
   -  **nft-based**<br/>
   grants voting power to those who own the expected NFT in their wallets
      
2. Voting method:
   - **binary**<br/>
   allows voting with the whole voting power at once on only one choice
   - **quadratic**<br/>
   allows voting on multiple choices while keeping the rule of quadratic voting at the same time (if you're not familiar with the idea, check it out [here](/docs/Welcome/Analytics/votemarketsreport#what-is-quadratic-voting))

3. Vote power calculating method:
   - **aggregated**<br/>
   it allows voting per wallet, meaning the user might vote only once, using concatenated assets at once
   - **not-aggregated**<br/>
   it allows voting per NFT ID, meaning each identity might vote differently 
