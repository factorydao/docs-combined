---
sidebar_position: 2
id: tasksetup
title: Task Setup
---
### How to set up a proposal?
##### For Admins and members
---
### Entering New Task Mode
1. On the main page choose interesting you realm.
2. Hit ``+`` button above the proposals list (only moderators can add new proposal).

<p align="center">
<img width="750px" alt="entering new proposal creating form" src={require("../../../../../static/images-influence/new_task.jpg").default}/>
</p>

## Create new task
<p align="center"><img src={require("../../../../../static/images-influence/task_creation.jpg").default}/></p>


Form fields:
- Title & description
- Start & end dates 
- Ranking end date - more on ranking further on
- Max submissions length - how many symbols user may input into submission form
- Task type - text, media, mixed ([more on task types](/docs/suite-docs/influence/contents/users/newsubmission))
- Tag - you can choose tag from the dropdown or create your own ([more on tags in the proposals view details](/docs/suite-docs/influence/contents/admins/proposalsetup#proposal-details))
- 'Open to' box fields - decide how many submissions can be added (more on that further on)


## Ranking
The ranking is a special task mode that starts right after the task end and lasts till the ranking end date set on task creation is reached. <br/>
From task start till the ranking end, users may vote on the submitted submissions to decide which are the best, but after the task end cannot add any new ones. So ranking mode is basically created to give chance to those submissions that were added near the task end, so they can gain some votes too. 

**Voting on submissions:**

<div class="tableWrapper">
    <video controls src={require("../../../../../static/images-influence/video/voting_submission.mp4").default} width="680"></video>  
    </div>
    <br/>


## Submissions limits
On task creation you can decide how many submissions the user may add to the task and what kind of assets are allowed - NFT, Token or both.

You cannot decide which NFT or which Tokens are allowed, you only decide on asset type. The system takes all assets that are allowed within that realm.<br/>
For some realms, multiple strategies are implemented, and some of them may be using different NFTs. <br/>
Since tasks are insensitive to strategies, there is no way to whitelist or blacklist any specific NFTs contract addresses. <br/>