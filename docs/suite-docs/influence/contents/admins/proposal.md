---
sidebar_position: 1
id: proposalsetup
title: Proposal Setup
---
### How to set up a proposal?
##### For Admins and members
---
### Entering New Proposal Mode
1. On the main page choose interesting you realm.
2. Hit ``+`` button above the proposals list (only moderators can add new proposal).

<p align="center">
<img width="750px" alt="entering new proposal creating form" src={require("../../../../../static/images-influence/new_proposal.jpg").default}/>
</p>

  
## Steps to Creating Proposal
A quick reminder on how to properly set up a proposal. <br/>
If you are not familiar with any (or some) of the points, we encourage you to read more in the further parts of this article. 

<div class="noMargins">

1. Hit **New Proposal** button.
2. Fill in **title** ("Question" field) and **description** ("What is your proposal?"). You can use Markdown basic syntax to format your description ([read more](/docs/suite-docs/influence/contents/admins/markdown)).
3. Set **start** and **end** dates. 
4. Choose **strategy**.
5. Choose **proposal's tag**.
6. **Optional:** Decide on proposal's options (or leave them as are):
    - Which chains should be allowed
    - Turn on winnerbox
    - Restrict viewing results
    - Decide who is eligible to vote (whitelist) or/and who is ineligible (blacklist)

7. Fill in voting options (**choices**). You can choose to fill in **choices descriptions** as well. 
8. When everything is filled out we recommend to check everything because **once published there is no editing possibility**. You can decide to publish proposal in **draft mode**.
9.  After making sure everything is correct you can finally hit **Publish** button. If all required fields are filled in properly, *loading screen and signature request from MetaMask will appear*.
10. **All done!** Proposal has been made and you've been sent to the main page. Created proposal should appear on the top of the proposals list.
11. If you've created proposal in a **draft node**, decide on publishing it or deleting. 

</div>

## Proposal elements

<div class="sideTosideWrapper">
    <p align="left">
    When you first create a proposal You'll see this form: 
    </p>
    <p align="right">
    <img height="50" src={require("../../../../../static/images-influence/hoverPR.png").default}/>
    </p>
</div>


<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-influence/1_proposal_elements_hover.jpg").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-influence/1_proposal_empty.jpg").default}/>
    </div>
</div>

### Proposal content
Proposal content consists of the title ("Question") and the description ("What is your proposal?"). You can use Markdown basic syntax to format your description ([read more](/docs/suite-docs/influence/contents/admins/markdown)). <br/>
Fill in the title and description. Try to clearly describe what your question is about. <br/>
If you need more space to enter text, use the handle to expand the field. 

<div class="sideTosideWrapper">
<p align="left">
<img height="50" src={require("../../../../../static/images-influence/hover.png").default}/>
</p>

</div>


<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-influence/1_proposal_description_filled.jpg").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-influence/1_proposal_empty.jpg").default}/>
    </div>
</div>
<br/>
<br/>


:::note
Remember to use less than 40 000 characters in your proposal description
:::

### Proposal voting choices
Filling in at least 2 choices is mandatory. They should relate to the proposal description.<br/>
To add more choices hit ``Add choice`` button, to remove choice use ``X`` mark next to the choice you want to remove. <br/>
**Adding a choice description is optional.** Descriptions are accessible through the voting component. Choices that have one are clickable and highlighting (check out the video for reference).

<div class="videoWrapper">
<video controls src={require("../../../../../static/images-influence/video/choice_description.mp4").default} width="680"></video>  
</div>
<br/>
<br/>


<!-- FILLING CHOICES -->
<div class="sideTosideWrapper">
    <p align="left">
    filling choices
    </p>
    <p align="right">
    <img height="50" src={require("../../../../../static/images-influence/hoverPR.png").default}/>
    </p>
</div>



<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-influence/1_proposal_choices_filled.jpg").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-influence/1_proposal_empty.jpg").default}/>
    </div>
</div>
<br/>
<br/>



### Proposal details
In the 'Action' field (Proposal's details) you decide when proposal starts and ends, on what kind of strategy it will run and what will be the proposal's tag. Below the graphic, you'll find a more specific description of each parameter. 

<div class="sideTosideWrapper">
<p align="left">
<img height="50" src={require("../../../../../static/images-influence/hover.png").default}/>
</p>
</div>

<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-influence/1_proposal_details_section.jpg").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-influence/1_proposal_empty.jpg").default}/>
    </div>
</div>
<br/>
<br/>

Mandatory proposal's elements are:
- **start and end dates** <br/>
    The start and end time of the proposal must not be the same. <br/>
    The date set here will be the time set in the time zone specified under the calendar box.
  
    <div class="box" style={{gap: "10px", width: "90%", alignItems: "center", maxHeight: "400px"}}>
    <p align="left"><img height="20" src={require("../../../../../static/images-influence/startdate.jpg").default}/></p>
    <p align="right"><img height="20" src={require("../../../../../static/images-influence/time.jpg").default}/></p>
    </div>

- **strategy** - strategies are mechanics for proposals that determine the voting power of users and how they are allowed to cast their votes. Each strategy has different voting power calculations and relies on different sources (tokens, cryptocurrencies, NFTs). Each realm has a predefined set of strategies allowed to use in the proposals. 
- **tag** - the proposal tag consists of up to 4 characters. Tags make it easier to find proposals in the list as well as categorize and organize them.     
    * **You can choose from existing tags within dropdown menu** or create your own by hitting "Add new sign". Choosing to add your own sign, a new window will pop up with name field and color picker.

    <div class="box" style={{gap: "10px", width: "90%", alignItems: "center"}}>
    <p align="left"><img style={{maxHeight: "400px"}} src={require("../../../../../static/images-influence/newsign.jpg").default}/></p>
    <p align="right"><img style={{maxHeight: "400px"}} src={require("../../../../../static/images-influence/signpicker.jpg").default}/></p>
    </div>

- **snapshot** - it's the height of the block in the blockchain that was used to check account balance. 


There are also a few additional setting options that could be used to control who can see and/or vote in the proposal and whether the proposal's winning option should be especially exposed after the end:

- **acceptable chains** - as influence is a multichain platform, those checkboxes allow user to decide which chains should or shouldn't be considered within the proposal. Some of the strategies sums up user balance within all chains, so those checkboxes help control it. 
- **winnerbox** - it's a field that appears at the proposal's end and contains the name of the choice (or choices) that has won.


<p align="center"><img style={{width: "70%"}} src={require("../../../../../static/images-influence/winnerbox.jpg").default}/></p>
<p align="center"><img style={{width: "70%"}} src={require("../../../../../static/images-influence/3_way_tie.jpg").default}/></p>

- **viewing restriction** - it's an option that restricts the preview of voting results for those who didn't meet the prerequisites (having a specific token or NFT in the wallet, determined by the strategy). <br/> Simply put, it is an option that allows only those who are eligible to vote in this proposal to see the results.
- **voting restrictions** are handled per chain (for example one address/token ID could be restricted only on mainnet but not on other chains). You can either type ID number/wallet address directly into the field or paste CSV formatted array:
    
    - **whitelist** - a list of IDs numbers or wallet addresses (**depending on strategy**) that are eligible to cast a vote:

<div class="videoWrapper">
    <video controls src={require("../../../../../static/images-influence/video/whitelist.mp4").default} width="680"></video>  
    </div>
    <br/>

- 
    - **blacklist** - a list of IDs numbers or wallet addresses (**depending on strategy**) that are ineligible to cast a vote:

<div class="videoWrapper">
    <video controls src={require("../../../../../static/images-influence/video/blacklist.mp4").default}></video>  
    </div>
    <br/>

- 
    - **Hide votes?** - Option to hide the results of a vote until it is finished
    - **Hackathon view** - changes the type of voting component, allowing more information to be displayed in the extended choice preview and the list of choices:

    <div class="sideTosideWrapperIMG500" style={{justifyContent: "flex-end"}}>
    <p>Hackathon choice form</p>
    <p align="left">
    <img width="500" src={require("../../../../../static/images-influence/1_hackathon_choice_empty.png").default}/>
    </p>
    </div>
    <br/>
    <div class="sideTosideWrapperIMG500" style={{justifyContent: "flex-end"}}>
    <p>Hackathon voting <br/> component</p>
    <p align="left">
    <img width="500" src={require("../../../../../static/images-influence/1_hackathon_voting_component.png").default}/>
    </p>
    </div>
    <br/>
    <div class="sideTosideWrapperIMG500" style={{justifyContent: "flex-end"}}>
    <p>Hackathon choice expand</p>
    <p align="left">
    <img width="500" src={require("../../../../../static/images-influence/1_hackathon_voting_expand.png").default}/>
    </p>
    </div>



### Draft mode
Before you publish your proposal you may want to share it with someone so they double check it. But since the proposal cannot be changed or deleted after publishing, you can publish a draft of it first. <br/>
All you have to do to achieve that mode is to simply leave the draft switch 'On' during proposal creating:

<p align="center"><img style={{width: "350px"}} src={require("../../../../../static/images-influence/draft.jpg").default}/></p>



You may now share the draft with someone and be sure that no one will be able to vote while the proposal is in draft mode. 

Once you'll decide that everything is correct, you can publish it (square with arrow icon) or if there were some mistakes, you can delete it (bin icon):

<p align="center"><img style={{width: "200px"}} src={require("../../../../../static/images-influence/draft_icons.jpg").default}/></p>


If you don't want to publish a proposal in a draft mode, remember to turn the draft switch 'Off' during proposal creation:

<p align="center"><img style={{width: "350px"}} src={require("../../../../../static/images-influence/draft_off.jpg").default}/></p>










