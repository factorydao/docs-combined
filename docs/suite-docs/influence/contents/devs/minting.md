---
sidebar_position: 1
id: mintingIdentity
title: Minting Identity
---

Minting an identity is an anti-sybil protection.<br />

Learn more about [minting here](/docs/Welcome/Whitepaper/digitalidentity#if-you-cant-beat-them-join-them).


## How does it work?
In purpose of voting, first thing to do is to mint an identity in cost of burning some [$FVT](/docs/Welcome/FactoryDAO/fvttoken) to gain voting power. <br />
And that is what it can be seen from user interface. But what is going on under the hood?

![Merkle Identity](../../../../../static/images/MerkleIdentity.jpg)

This scheme depicts mechanism standing behind minting an Identity within **Influence.vote**. 
<br />

- Our starting point is **GatedMerkleIdentity** contract, which remembers all Merkle trees root hashes and verifies whether Merkle proof delivered by user is in any of the Merkle trees.<br />
More on [Merkle tree here](/docs/Welcome/Knowledge/merkletree).
Adding tree hashes to **GatedMerkleIdentity** requires passing NFT address and gate address, both of which could be get via contracts.

<br />

- **GatedMerkleIdentity** connects to the contract of token, which is desired by user to get.
In Influence.vote the only available token's contract right now is **VotingIdentity2**, which creates an Identity and sets NFT addresses for them, also it transfers NFT from one user to another and shows minter's NFT balance.<br /> 
More on [Identities here](/docs/Welcome/Whitepaper/tokeneconomics#identity-tokens).

<br />

- Price of minting an Identity is dynamic and determined by amount of Indentities already bought. That's why there are two price contracts. <br />
**FixedPriceGate** always returns basic price, while **LinearPriceGate** returns price increasing in time. <br />
**Incinerator** takes a token address and some ether. It uses the ether to purchase the token and burn them (remove them from supply permanently).
The last two contracts indicated in the schema are used for trading from a smart contract. <br />
**UniswapRouter** and **SushiswapRouter** are contracts that works nearly the same and are used to safely swap tokens to and from different assets.
