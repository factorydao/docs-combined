---
sidebar_position: 1
id: settingup
title: Setting Up
---

# Setting up influence locally 
- **Frontend** -> [influence](https://gitlab.com/factorydao/influence)
- **Backend**  -> [influence-hub](https://gitlab.com/factorydao/influence-hub)

## Frontend

Create a `env.local` file in the main project directory. Paste there settings copied from `.env.example.` <br />
If you’d like to use local backend, remember to comment this line: 
```jsx title=".env.local"
REACT_APP_API_URL='https://next.influencebackend.xyz'
```

### Install and run
```
git checkout develop

npm install
npm run build
npm start
```

## Backend

In the main influence-hub folder create .env.local file and put in there settings copied from .env.
Add your wallet address into admin_init file under migration folder. Make sure export line is uncommented.
```
git checkout develop

npm install
docker-compose up --build
```
<br />

:::note
  If there are no documents in the database under strategies collection after the building, make sure `strategies_init` file under the migration folder has uncommented export line.
:::