---
sidebar_position: 1
id: proposal-explained
title: What elements does the proposal contain?
---
## Proposal view
Main view of proposal contains a title and a description with all the information about ballot and ballot's choices. <br/>
Details contain proposal hash (IPFS field), voting strategy, start and end voting date, block height (Snapshot field), countdown timer and voting result data in JSON and CSV.<br/>
Below that, there are four tabs:
* My vote
* Vote Results
* Opinions
* Votes list (and submitted votes counter)

## Tabs
### My Vote 
In the "My Vote" tab, you will find the following features:
- **Identity Picker**: This feature is available only in proposals that implement multi-identity strategies. It allows you to choose your identity from the available options before casting your vote.

- **Account Available Votes**: This section displays the number of votes allocated to your account that you can distribute among the choices.

- **Distribution of Votes**: In this field, you can specify how you want to distribute your allocated votes among the available choices. You can adjust the distribution according to your preferences.

- **Vote Form**: The vote form enables you to enter your votes and submit them for the chosen choices. This is where you can finalize your voting decisions and make your voice heard.

<div class="sideTosideWrapper">
<p align="left">
<img height="50" src={require("../../../../../static/images-influence/hover.png").default}/>
</p>
</div>

<div class="templ">
    <div class="templhover" style={{width: "90%", marginRight:"20%"}}>
    <img src={require("../../../../../static/images-influence/1_proposal_view.png").default}/>
    </div>
    <div class="templbasic" style={{width: "90%", marginRight:"20%"}}>
    <img src={require("../../../../../static/images-influence/1_proposal_view.jpg").default}/>
    </div>
</div>
<br/>


### Vote Results 
Vote results tab shows current or final results of the ballot.<br/>
If the winnerbox were switched 'on' during proposal creating, it will be shown here at the proposal end. 

<p align="center"><img style={{width: "70%"}} src={require("../../../../../static/images-influence/2_votes_tab.jpg").default}/></p>


### Opinions 
In the "Opinions" tab, you can view justifications provided by other users for their submitted votes. To make it easier to find specific choices, you can use the filtering option by choice names. This allows you to narrow down the displayed opinions to only the choices that interest you.

Additionally, there are sorting options available for both all opinions and the filtered choices. You can sort the opinions by either the oldest or newest submissions, as well as by the "$I Power" metric, which indicates the influence or weight of the opinion. This enables you to arrange the opinions based on different criteria and preferences.
<p align="center"><img style={{width: "70%"}} src={require("../../../../../static/images-influence/2_opinions_tab.jpg").default}/></p>

### Votes list 
The "Votes List" tab provides information about how much voting power has been utilized by each token ID or wallet address (depending on the strategy). This tab gives you a clear overview of the distribution of voting power among different participants, allowing you to see how much influence each entity has in the voting process.

<p align="center"><img style={{width: "60%"}} src={require("../../../../../static/images-influence/5_votes_list.jpg").default}/></p>



