---
sidebar_position: 2
id: voting
title: How to Vote?
---
#### How to vote in a proposal?
##### For Users
---

1. Connect your wallet.
2. Enter proposal and read its description.
3. If needed, make sure you're on proper chain.
4. Allocate your votes (quadratic strategies) or choose an option (binary strategies).
5. Hit **Vote** button.
6. Justify your vote (optional).
7. Hit **Submit vote** button.
8. Confirm voting in MetaMask.<br/>

Voila! 

If you're still confused, checkout **a quick walkthrough video** down below:

<div class="tableWrapper">
    <video controls src={require("../../../../../static/images-influence/video/voting.mp4").default} width="680"></video>  
    </div>
    <br/>
    <br/>


:::info Voting notes
<div style={{verticalAlign: "middle", display: "inline"}}><div>To submit your vote, click on the voting field -> <img style={{width: "100px"}} src={require("../../../../../static/images-influence/vote.jpg").default}/> and enter a numerical value. <br/>
Alternatively, you can use the arrows that appear next to the field. <br/><br/>
<strong>Our voting system follows a quadratic voting approach, where the value you enter is squared. </strong> This means that you can allocate more than one vote to a specific choice, but the stronger or more valuable your votes become, the more they will cost you in terms of voting power. <br/>
You have the flexibility to use your vote power in different ways. You can either spend all of your vote power on a single choice or distribute it across multiple available choices. However, it's important to ensure that the total value of your votes doesn't exceed your allocated vote power.
</div></div>
<br/>
In some cases, depending on the strategy employed, there is a possibility of **negative voting**. If you strongly oppose a particular choice, you can attempt to weaken its position by entering a negative value. Please note that the entered value will still be squared and deducted from your overall vote power.
<br/><br/>
 <p align="center"><img style={{width: "70%"}} src={require("../../../../../static/images-influence/6_negative_vote.jpg").default}/></p>
:::

  




