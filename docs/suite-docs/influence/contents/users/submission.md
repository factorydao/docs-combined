---
sidebar_position: 3
id: newsubmission
title: Contribute Submission
---
#### How to add a submission?
##### For Users
---
Tasks are a unique type of proposal where users are presented with a question, and their role is to propose an answer.<br/>
The configuration of the task determines whether users can submit more than one answer per ID, whether it's a token or an NFT. This flexibility allows users to provide multiple submissions if permitted by the task settings.

**Task types:**
1. text - only text submissions
2. media - only graphic/video submissions
3. mixed - allows to send text and graphic/video simultaneously 

**Steps:**
1. Connect your wallet.
2. Enter task and read its description.
3. Hit **Create a submission** button.
4. Choose ID you want to send your proposal with.
5. Click **Next**.
6. Add **Title and content** (depend on task type).
7. Hit **Submit vote** button.
8. Confirm in MetaMask.<br/>

Voila! 

## Text submissions
In a proposal, the title and description are required fields that must be filled out. <br/>
The length of the description may vary based on the configuration of the task.

## Media submissions
Influence supports various media types such as YouTube, Vimeo, TikTok, Twitter URLs, as well as accepting images in formats like **.jpeg, .jpg, .webp, .png,** and **.gif.**<br/>
When creating a submission, you have the option to choose the type of media you would like to include. You can either paste the URL of the media or send an image directly. This flexibility allows you to enhance your submission with the desired media content.

## Mixed submissions
This type provides the highest level of freedom. You have the option to add media, text, or both simultaneously, based on your preference.<br/>
The only requirement is that you must include at least something in your submission, along with a title. This ensures that there is some content accompanying the proposal and allows for a more comprehensive submission.