---
sidebar_position: 1
---

# Welcome to Markets

markets is a prediction market and collective intelligence tool, which uses quadratic voting and a decentralised identity system to curate emerging markets and reach consensus on the perceived future value of assets. It allows users to break through the noise of permissionless systems and concentrate on the assets most worthy of their attention.


## What is Markets for?

