---
sidebar_position: 1
id: marketssetup
title: Markets Setup
---
Repositories:<br/>
[Contracts](https://gitlab.com/finance.vote/contracts)

[Trollbox](https://gitlab.com/finance.vote/trollbox)

## Setting up
Prerequisites:

* Created an account on GitLab via invite
* Code editor installed
* On MacOS: wget install

Testing server address: 207.154.210.94:8545<br/>
1. In the main trollbox folder create a file **.env.local** and put in there:
```jsc title=".env.local"
REACT_APP_LOCAL_PROVIDER_URL='http://207.154.210.94:8545’
```

2. Update contracts, see [here](/docs/suite-docs/markets/contents/Configure/contractsupdate).

3. In trollbox terminal run:
```
git checkout develop
git pull
yarn install
yarn start
```


