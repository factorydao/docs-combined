---
sidebar_position: 2
id: deploymintcontracts
title: Deploying contracts
---
To set up a mint, several contracts are needed to be deployed in the right order.

1. MerkleIdentity.sol::

```
    address _mgmt atribute -> creator wallet address
```

2. VoterID.sol::
```
    ooner atribute -> creator wallet address
    minter atribute -> MerkleIdentity contract address
    nomen atribute -> name of your choice
    symbowl atribute -> symbol of your choice
```

3. AmaluEligibility.sol or MerkleEligibility.sol::
```
    AmaluEligibility:
    _mgmt atribute -> creator wallet address
    _gatemaster atribute -> MerkleIdentity contract address

    MerkleEligibility:
    _gatemaster atribute -> MerkleIdentity contract address
```

4. AmaluPriceGate.sol or FixedPricePassThruGate.sol or SpeedBumpPriceGate.sol


:::warning Be careful
**NFT** address = **VoterId** contract address<br/>
**GATE** address = **MerkleIdentity** contract address
:::

