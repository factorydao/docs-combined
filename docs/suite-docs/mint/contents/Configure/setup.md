---
sidebar_position: 1
id: mintsetup
title: Mint Setup
---
## Frontend

[Repository](https://gitlab.com/finance.vote/factory-dao-mint.git)

In the mint terminal:
```
git checkout develop

npm install
npm start
```




## Backend

[Repository](https://gitlab.com/finance.vote/minting-backend.git)

In the minting-backend repo:
```
nvm use
npm i 
docker-compose up --build
```