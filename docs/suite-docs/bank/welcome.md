---
sidebar_position: 1
---

# Welcome to Bank

bank provides DAOs with secure scalable infrastructure for token vesting. Token recipients receive tokens over a period of time at a fixed rate per second, improving the incentive structure for DeFi projects. This can be used for investors, founders, employees, and contractors.


## What is bank for?

