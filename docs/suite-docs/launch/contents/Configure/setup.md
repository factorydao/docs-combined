---
sidebar_position: 1
id: launchsetup
title: Launch Setup
---
[Repository](https://gitlab.com/factorydao/token-auction) 

In the launch terminal:
```
git checkout develop

npm install
npm start
```