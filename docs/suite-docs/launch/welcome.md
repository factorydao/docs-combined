---
sidebar_position: 1
---

# Welcome to Launch

launch is a non-custodial crypto auction house that uses novel auction mechanisms to find the optimal price for both fungible and non-fungible digital assets. Auction is designed for price discovery and reaching consensus on the value of new or highly subjective digital assets.


## What is launch for?

