---
sidebar_position: 3
id: residentpool
title: Resident Pool
---
## Resident Pools
Once you've learned how Basic Pool works, it shouldn't be too hard to understand how Resident Pool works. 

Resident Pool is a new kind of liquidity mining.<br/>
As a user you will gain access to network emission by holding a protocol property.<br/>
Protocol property is a slot in a smart contract that provides you access to the liquidity incentive token inventory of the finance.vote protocol.<br/>
Resident Pool uses ‘Harberger Taxes’ to broker ownership of these slots in an on-going manner.<br/>

<div class="box">
<p align="right" style={{width:"100%"}}>
<img style={{height:"50px"}} src={require("../../../../../static/images-yield/hoverPR.png").default}/>
</p>
</div>

<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-yield/3_residenthover.png").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-yield/3_residenthover.jpg").default}/>
    </div>
</div>
<br/>
<br/>


Hovering on the image above releases another graphics but you shouldn't be surprised with this anymore.<br/>
Again, let's decrypt every element of this site, as we did before:


1. On the left side there is a menu panel with shortcuts to all available pools. 
2. Right below the name of the pool, there are 2 parameters:
    * **Total Liquidity** is a total amount of LP Tokens staked in the pool (from all user total). 
    * **Average APE PY** stands for “Average Pulse Expected Percentage Yield”. 
    * **Average APY** stands for “Average Annual Percentage Yield”. 
3. Pool details field
   * **Volume** - how much LP Tokens where staked in the last 24 hours (from all user total)
   * **Minimum Deposit** - minimum amount of LP Tokens that can be staked 
   * **Maximum Deposit** - maximum amount of LP Tokens that can be staked 
   * **Lots filled** - number of currently occupying lots
   * **Current Pulse Length** - how many blocks will be in the current pulse
   * **Slot Rewards / Pulse** - rewards distributed per pulse
4. Below Pool details there is a field with two information:
    * **Lots** - number of yours occupied lots.
    * **Total rewards** - a sum of rewards from all your currently occupying lots. If you'll pay out any rewards *total rewards* won't change, but will if you'll vacate a Lot or get kicked out from it (*Total rewards* decrease by the number of total rewards from that Lot). 
5. On the right side there is a chart showing rewards in the current pulse. Since it’s not a flat rate, it starts off higher and then lowers quadratically. So the earlier you get in, the more you’ll earn.
6. Last element is the list of Occupied Lots. Here you can see every occupied Lot, regardless of whether it is occupied by you or not. To see only yours lots (if you have any) choose My Lots tab. Elements of list of Occupied lots:
    * **Lot No** - occupied property number
    * **Occupant** - wallet address of occupant
    * **Deposit** - current Lot price (initial deposit constantly decreasing by burn rate per pulse). This is also minimal amount of LP Tokens required to claim this Lot
    * **Burn rate** - amount of LP tokens that are burned per pulse
    * **Rewards** - the amount of profit from the property occupying. It is calculated from the start of Lot occupying and if you withdraw some funds during occupying the total amount won't change.
    * **APEPY**  - Average Pulse Expected Percentage Yield for the specific Lot
    * **Claim Lot** - button used for taking over the Lot
    * **Withdraw** (button shown only on your Lots) - button used for withdraw current available rewards

<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/3_residentlot.jpg").default}/>
    </p>


### Staking LP Tokens (Occupying Lot)
Before we dive into this, you can see how Resident Pool works in Dr Nick's short overview video:

<p align="center">
<iframe width="560"
    height="315"
    src="https://www.youtube.com/embed/wxWCjCcyw1c"
    title="YouTube video player"
    style={{border:"4px solid #96E048"}}
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
    
To become a property owner you'll have to stake LP tokens. We assume you've already have them (if not, see here [how to gain some](https://financevote.readthedocs.io/projects/yield/en/latest/content/token.html)).

1. Hit ``Stake LP Tokens`` button.
2. From the left column choose number of Lot you would like to occupy (every number has its own personalized image).

<p align="center">
<div class="templ">
    <div class="templhover">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking4.png").default}/>
    </div>
    <div class="templbasic">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking4.jpg").default}/>
    </div>
</div>
</p>
<br/>
    
   You can occupy either vacant or already occupied Lot. The difference between them is initial <strong>minimum LP tokens deposit</strong> and initial <strong>Burn Rate</strong>. With the vacant Lots, those indexes are set by pool. With the occupied ones they are set by occupant and you have to outbid both of that to take over the Lot.

<p align="center">
<div class="templ">
    <div class="templhover">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking1.png").default}/>
    </div>
    <div class="templbasic">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking1.jpg").default}/>
    </div>
</div>
</p>
<br/>

3. Set Deposit Amount (or New Deposit in occupied Lot).
4. Set Burn Rate (or New Burn Rate in occupied Lot). This index is the amount of LP tokens that are burned per pulse and it is strictly connected with the ‘Harberger Tax’. 

<p align="center">
<div class="templ">
    <div class="templhover">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking2.png").default}/>
    </div>
    <div class="templbasic">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking2.jpg").default}/>
    </div>
</div>
</p>
<br/>


5. Current APEPY (or New APEPY) will calculate automatically. If you are not satisfied with its rate, you can try to set different Burn Rate. *hint: the smaller Burn Rate, the better APEPY is*.
6. Hit ``Claim Lot`` button. 
7. Confirm transaction fees in MetaMask pop up window.
8. If no error occurs, ``A deposit has been made!`` notification will pop up. 
9. Claimed Lot will be shown on the list in My Lots tab.

<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/4_staking3.jpg").default}/>
    </p>


### Withdraw or Vacate 
1. Press withdraw button

<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/5_withdrawal.jpg").default}/>
    </p>


2. Window with details will pop up. Here you can check what was your **initial deposit**, how much LP was **burned** till now and how many **blocks till the end of current pulse** left. 
You can either **withdraw** available rewards or **vacate the lot**. When you vacate the lot, you gain available rewards and all **unburned LP that has left** (if there is any). Withdrawal lets you gain rewards while keeping the lot.

<p align="center">
<div class="templ">
    <div class="templhover">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/5_withdrawal2.png").default}/>
    </div>
    <div class="templbasic">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/5_withdrawal2.jpg").default}/>
    </div>
</div>
</p>
<br/>

3. Press ``Withdraw Rewards`` or ``Vacate Lot`` button.
4. Confirm transaction fees in MetaMask pop up window.
5. If no error occurs, ``Successful payout!`` or ``Successful vacate!`` notification will pop up. Either option, **Total Rewards** field will enlarge by the amount of gained rewards. If you chose to Vacate option, besides **Total Rewards** enlarge, **the Lots counter** will also decrease by 1.

<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/5_totalrewards.jpg").default}/>
    </p>
<br/>
<p align="center">
<div class="templ">
    <div class="templhover">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/5_withdrawaltotal2.png").default}/>
    </div>
    <div class="templbasic">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/5_withdrawaltotal.jpg").default}/>
    </div>
</div>
</p>
<br/>

 