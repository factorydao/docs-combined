---
sidebar_position: 1
id: aboutyield
title: About yield
---
If you felt confused on yield.vote site then you are in the right place.<br/>
We'll try to simply explain how to play and to not get confused again.<br/>

<div class="sideTosideWrapper">
    <p align="left">
    This is what you see right after entering yield: 
    </p>
    <p align="right">
    <img height="50" src={require("../../../../../static/images-yield/hoverPR.png").default}/>
    </p>
</div>


<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-yield/mainpage_1.png").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-yield/mainpage_1.jpg").default}/>
    </div>
</div>
<br/>
<br/>

    
We truly hope that this part is not confusing, but to be absolute clear, let us explain what are you looking at.

The most important thing here is to understand that this is kind of a template which will be created for every partner who would like to create its own pools.<br/>
In the finance.vote pools section there are 3 boxes for 3 different types of pools. We called them basic, resident and tycoon(which is actually a type of resident pool).<br/>
As you can see, our partner's pools are named completely different (ITG Blue Lagoon Pool, ITG Reef Pool and ITG Krill Pool) but their working is based on the same mechanics as finance.vote pools. 

<div class="sideTosideWrapper">
<p align="left">
<img height="50" src={require("../../../../../static/images-yield/hover.png").default}/>
</p>
</div>

<div class="templ">
    <div class="templhover">
    <img src={require("../../../../../static/images-yield/1_mainppage_templ2.jpg").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-yield/1_mainppage_templ.jpg").default}/>
    </div>
</div>
<br/>
<br/>


If you want to find out how our pools work, take a look at the documentation:
1. [Basic pool](/docs/suite-docs/yield/contents/Usage/basicpool)
2. [Resident pool](/docs/suite-docs/yield/contents/Usage/residentpool)
3. Tycoon pool is not available yet :)