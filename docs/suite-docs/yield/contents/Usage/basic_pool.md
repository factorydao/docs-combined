---
sidebar_position: 2
id: basicpool
title: Basic Pool
---
When you first enter basic pool you probably think that there is too much information. But keep calm, we will go through this together. <br/>
Actually it is not that complicated, since most of the elements are just informational, but yes, that could be overwhelming at first. 
 
Basic pool is an ultra simple financial primitive for those who don’t want to be LPs, but want to get FVT token yield.<br/>
The contract opens at a specified time(**start date**) and reaches maturity at a specified time(**maturity date**). You see a quote for the yield(**APY**) you will receive on maturity and as long as you send your tokens before the pool cap is full, you get your quoted yield guaranteed from the timestamp of the block in which your transaction is mined.


<div class="sideTosideWrapper">
    <p align="left">
    Let's take a look on Basic Pool site:  
    </p>
    <p align="right">
    <img height="50" src={require("../../../../../static/images-yield/hoverPR.png").default}/>
    </p>
</div>


<div class="templ">
    <div class="templhover + templhover-basic">
    <img src={require("../../../../../static/images-yield/2_basic1.png").default}/>
    </div>
    <div class="templbasic">
    <img src={require("../../../../../static/images-yield/2_basic1.jpg").default}/>
    </div>
</div>
<br/>
<br/>

Hovering on the image releases another graphics which probably is a little disturbing for now, but don’t worry, once you’ll understand what's what, the schema becomes more helpful.<br/>
Let's start decrypting every element of the site:

1. On the left side there is a menu panel with shortcuts to all available pools. 
2. Right below the name of the pool, there are 3 parameters:
    * **Total Value Locked** is a total amount of FVT currently deposited in the pool (from all user total). 
    * **APY** stands for “Annual Percentage Yield”. It is the amount of yield you would get on your principal (what you put in) annualised. In Basic Pool, your quotes are literal. What you see is what you get.
    * **Filled** stands for percentage of current pool filling (from all user total).
3. Pool details field
   * **Maximum Pool Size** - information on the pool size, meaning how much FVT can be deposit in the pool 
   * **Volume** - information on how much FVT where deposited into the pool in the last 24 hours (from all user total)
   * **Current Yield** - information on current percentage (how much user will get at the maturation). **(!) This parameter decreases with pool maturing.**
   * **Start Date** - time when contract opens
   * **Maturity Date** - time when contract reaches maturity and gets closed
4. Deposit field is something like a bank window, where you cen deposit your FVT and send them to the pool. ``MAX`` button allows deposit a maximum allowed amount of FVT, considering your FVT balance and pool capacity. 
5. List of open and closed transactions shows only yours open and closed positions (not other users):
- Open Positions elements:
    * **Transaction** - hash of deposit transaction 
    * **Deposit** - amount of FVT deposited
    * **Current Yield** - amount of yield that has grown till this moment (what you would get right now if you would make a withdrawal)
    * **Additional Rewards** - amount of any additional rewards
    * **Yield at Maturity** - amount of the yield you will get when the pool reaches maturity
    * **Withdraw** button used for funds withdrawals
    
     <p align="center">
    <img style={{width:"90%"}} src={require("../../../../../static/images-yield/2_basic_example4.jpg").default}/>
    </p>


- Closed Positions elements:
    * **Deposit Transaction** - hash of deposit transaction
    * **Deposit** - amount of FVT deposited
    * **Yield** - amount of yield collected
    * **Withdraw Transaction** - hash of withdraw transaction

    <p align="center">
    <img style={{width:"90%"}} src={require("../../../../../static/images-yield/2_basic_example5.jpg").default}/>
    </p>

    
    

### Deposit into Basic Pool
1. Enter deposit amount or use MAX button.
2. Hit Deposit button.
3. Confirm transaction fees in MetaMask pop up window.
4. If no error occurs, ``A deposit has been made!`` notification will pop up. 
5. Finished transaction will be shown on list of open transactions. 

**Let's explain this more detailed over an example:**

:::tip Example
Carla enters the Basic Pool and she sees that maximum pool size is 10,000,000 FVT but currently only 43,672 FVT are deposited into the pool (and that's 0.437% filled). 

She sees that if she deposit now her yield at pool maturation will be 12.93%, so she decides to deposit 100 FVT (which will give her additional 12.93 FVT at pool maturity).
<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/2_basic_example2.jpg").default}/>
    </p>
    
She enters 100 FVT into Deposit Amount field and hits Deposit button. <br/>
MetaMask notification pops up and she has to confirm transaction fees.

She waits a while till deposit is send to the pool and then she can see her transaction on the list of open transactions. 
<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/2_basic_example3.jpg").default}/>
    </p>
    
She also noticed that Volume field and Total Value Locked (TVL) has changed and now are enlarged by her deposit. 
:::


### Withdraw from Basic Pool
1. Hit Withdraw button on the open positions list.
2. Information on deposit funds, current yield, yield at pool maturity and additional rewards will be shown as a pop up window. Read them carefully and keep in mind that once you'll withdraw your funds you won't be able to enter the pool on the same conditions again. 
3. Click Withdraw.
4. Confirm transaction fees in MetaMask pop up window.
5. If no error occurs, ``Successful payout`` notification will pop up.
6. Withdrawn position will be shown on list of closed positions.

**Let's explain this more detailed over an example:**
:::tip Example
Carl entered the Basic Pool much sooner than Carla, that's why his yield at maturity percent was higher (14.23%). Now he wants to withdraw his funds from the pool.
<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/2_basic_withdraw1.jpg").default}/>
    </p>

He hits Withdraw button on the open positions list, which opens a pop up window with information on deposit funds, current yield, yield at pool maturity and additional rewards. He should take a minute and think whether he really needs to take out his fund because once he'll done it, there will be no coming back. And what's more, any further deposits will be less profitable since yield percent decreases as pool matures. 
<p align="center">
    <img style={{width:"70%"}} src={require("../../../../../static/images-yield/2_basic_withdraw2.jpg").default}/>
    </p>

Carl however is decided and clicks Withdraw button and confirms transaction fees in popped up MetMask window. 
    
He waits a while and then he sees that his transaction disappeared from open transactions list but appeared on the closed positions list instead. 

<div class="box" style={{gap: "10px", width: "90%", alignItems: "center"}}>
<p align="left"><img style={{maxHeight: "400px"}} src={require("../../../../../static/images-yield/2_basic_withdraw3.jpg").default}/></p>
<p align="right"><img style={{maxHeight: "400px"}} src={require("../../../../../static/images-yield/2_basic_withdraw4.jpg").default}/></p>
</div>

He also noticed that Total Value Locked (TVL) has changed and now is reduced by his withdrawal.  
:::

