---
sidebar_position: 1
---

# Welcome to Yield

yield is a liquidity mining and staking platform that provides users of token ecosystems with yield opportunities in return for supporting the creation of decentralised markets. Yield has tuneable governance parameters that can be modified by a DAO.


## What is yield for?

