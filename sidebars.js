/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  // By default, Docusaurus generates a sidebar from the docs folder structure
  start: [{type: 'autogenerated', dirName: 'Welcome'}],
  influence: [{type: 'autogenerated', dirName: 'suite-docs/influence'}],
  bank: [{type: 'autogenerated', dirName: 'suite-docs/bank'}],
  mint: [{type: 'autogenerated', dirName: 'suite-docs/mint'}],
  yield: [{type: 'autogenerated', dirName: 'suite-docs/yield'}],
  launch: [{type: 'autogenerated', dirName: 'suite-docs/launch'}],
  markets: [{type: 'autogenerated', dirName: 'suite-docs/markets'}],

  // But you can create a sidebar manually
  
  // tutorialSidebar: [
  //   'intro',
  //   'hello',
  //   {
  //     type: 'category',
  //     label: 'Tutorial',
  //     items: ['tutorial-basics/create-a-document'],
  //   },
  // ],
   
};

module.exports = sidebars;
